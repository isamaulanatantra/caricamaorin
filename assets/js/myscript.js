const flashData = $('.flash-data').data('flashdata');

if (flashData) {
    Swal({
        title: 'Data',
        text: 'Berhasil ' + flashData,
        type: 'success'
    });
}

// tombol-hapus
$('.tombol-hapus').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal({
        title: 'Apakah anda yakin',
        text: "data akan dihapus",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus Data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })

});

// tombol-logout
$('.tombol-keluar').on('click', function (e) {

    e.preventDefault();
    var base_url = window.location.origin;

    Swal({
        title: 'Yakin keluar dari aplikasi?',
        text: "",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Keluar'
    }).then((result) => {
        if (result.value) {
            document.location.href = base_url+'/maorin/auth/logout';
        }
    })

});