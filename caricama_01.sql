-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 24, 2021 at 08:10 AM
-- Server version: 10.1.48-MariaDB-cll-lve
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `caricama_01`
--

-- --------------------------------------------------------

--
-- Table structure for table `bbaku`
--

CREATE TABLE `bbaku` (
  `idBaku` int(11) NOT NULL,
  `kategori` varchar(155) NOT NULL,
  `kode` varchar(155) NOT NULL,
  `produk` varchar(155) NOT NULL,
  `satuan` varchar(155) NOT NULL,
  `status` int(11) NOT NULL,
  `last_edited` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bjadi`
--

CREATE TABLE `bjadi` (
  `idBarang` int(11) NOT NULL,
  `namaBarang` varchar(255) NOT NULL,
  `satuan` varchar(255) NOT NULL,
  `tanggalUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `kategori` varchar(155) NOT NULL,
  `kode` varchar(155) NOT NULL,
  `status` int(11) NOT NULL,
  `min_harga` int(11) NOT NULL,
  `max_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bjadi`
--

INSERT INTO `bjadi` (`idBarang`, `namaBarang`, `satuan`, `tanggalUpdate`, `kategori`, `kode`, `status`, `min_harga`, `max_harga`) VALUES
(16, 'Carica Segar', 'Pcs', '2020-10-03 01:36:13', 'Kemasan', 'ADM', 1, 0, 0),
(17, 'Carica Enak', 'Pcs', '2020-11-09 10:06:50', 'Kemasan', 'MDA', 1, 5000, 7000),
(18, 'Carica Plekentung', 'Pcs', '2020-10-03 16:22:46', 'Kemasan', 'AAA', 1, 0, 0),
(19, 'Maorin Carica', 'Pcs', '2020-11-09 10:15:39', 'Kemasan', 'LLL', 1, 7500, 9000);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` enum('Active','Inactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `status`) VALUES
(1, 'Mangga', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id_pegawai` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenisKelamin` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kontak` varchar(255) NOT NULL,
  `tanggalInsert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(11) NOT NULL,
  `tanggalHapus` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_pegawai`, `nama`, `jenisKelamin`, `jabatan`, `alamat`, `kontak`, `tanggalInsert`, `status`, `tanggalHapus`) VALUES
(10, 'Maulana', 'Laki - Laki', 'Owner', 'Wonosobo', '0855', '2020-09-14 09:35:46', '0', '2020-09-14 16:37:02'),
(11, 'Joko', '', 'Driver', 'Jl. Sukmawati', '085555', '2020-09-21 11:42:46', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `katpelanggan`
--

CREATE TABLE `katpelanggan` (
  `id` int(11) NOT NULL,
  `kode` varchar(155) NOT NULL,
  `kategori` varchar(155) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `perubahanTerakhir` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `katpelanggan`
--

INSERT INTO `katpelanggan` (`id`, `kode`, `kategori`, `min`, `max`, `status`, `perubahanTerakhir`) VALUES
(4, 'D', 'Duduk', 15000, 16000, 0, '2020-09-26 02:37:08'),
(5, 'A3', 'Admin3', 15000, 16000, 1, '2020-11-09 10:03:30'),
(6, 'A', 'Ambil', 11000, 12000, 0, '2020-09-26 02:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id_kota` int(11) NOT NULL,
  `kode_prov` varchar(255) NOT NULL,
  `nama_kota` varchar(255) NOT NULL,
  `kode_idpel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id_kota`, `kode_prov`, `nama_kota`, `kode_idpel`) VALUES
(78, '33', 'Temanggung', 'TM'),
(79, 'Jawa Tengah', 'Magelang', 'MG'),
(80, 'Jawa Tengah', 'Semarang', 'SM'),
(81, 'Jawa Tengah', 'Wonosobo', 'WS');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `subcategory_id` int(11) NOT NULL,
  `subcategory_category_id` varchar(255) NOT NULL,
  `subcategory_name` varchar(255) NOT NULL,
  `alamat_toko` varchar(255) NOT NULL,
  `kodeToko` varchar(255) NOT NULL,
  `telp` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `namaPersonal` varchar(255) NOT NULL,
  `nowa` varchar(155) NOT NULL,
  `status` int(11) NOT NULL,
  `tanggalHapus` varchar(255) NOT NULL,
  `tanggalInsert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_transaksi` date DEFAULT NULL,
  `dibayar` int(11) NOT NULL,
  `lunas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`subcategory_id`, `subcategory_category_id`, `subcategory_name`, `alamat_toko`, `kodeToko`, `telp`, `email`, `kategori`, `foto`, `latitude`, `longitude`, `namaPersonal`, `nowa`, `status`, `tanggalHapus`, `tanggalInsert`, `tanggal_transaksi`, `dibayar`, `lunas`) VALUES
(1, 'Magelang', 'Maulana', 'Jalan Borobudur', 'DMG0001', '081111', 'a@a.com', 'Distributor', 'default.jpg', '-7.6090654', '110.2067608', 'Maulana', '081111', 1, '', '2020-11-24 02:50:03', '0000-00-00', 1275000, 1),
(7, 'Wonosobo', 'Aqua', 'Jalan Satu', 'AWS0002', '098798', 'a@asd.com', 'Agen', 'default.jpg', '-7.3575130999999985', '109.90598539999999', 'Aqua', '098798', 1, '', '2020-12-14 03:02:29', '2020-12-10', 14400, 1),
(8, 'Wonosobo', 'Asdghf', '', 'AWS0003', '', '', 'Agen', 'Number153.jpg', '-7.357511', '109.9059997', '', '', 1, '', '2020-12-14 03:02:36', '2020-12-10', 0, 1),
(9, 'Wonosobo', 'Tes Upload Foto', 'Jdkfhblakjsdhlfgads', 'AWS0004', '0894654', '', 'Agen', 'DSC_0011.JPG', '-7.150975', '110.14025939999999', 'Tes Upload Foto', '098746541', 1, '', '2020-12-14 12:51:22', '2020-12-14', 0, 0),
(10, 'Wonosobo', 'Jdfn', 'Ndfnknfdkj', 'DWS0005', '5155', 'asdmask@fskfms.cdm', 'Distributor', 'default.jpg', '', '', 'Fdngj', '5545', 1, '', '2021-01-23 06:54:25', '2021-01-06', 10000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `id_bjadi` varchar(110) NOT NULL,
  `id_pelanggan` varchar(255) NOT NULL,
  `satuan` varchar(110) NOT NULL,
  `harga` int(11) NOT NULL,
  `tanggalInsert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tanggalHapus` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id`, `id_bjadi`, `id_pelanggan`, `satuan`, `harga`, `tanggalInsert`, `tanggalHapus`, `status`, `jumlah`, `total`) VALUES
(1, 'Maorin Carica', 'DMG0001', 'PCS', 12500, '2020-11-18 02:25:06', '', 1, 50, 625000),
(2, 'Carica Enak', 'DMG0001', 'PCS', 13000, '2020-11-18 02:25:38', '', 1, 50, 650000),
(4, 'Carica Segar', 'AWS0002', 'PCS', 1200, '2020-12-10 06:20:46', '', 1, 12, 14400),
(6, 'Carica Segar', 'DWS0005', 'PCS', 2000, '2021-01-06 08:42:25', '', 1, 5, 10000);

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id_prov` int(11) NOT NULL,
  `kode_prov` int(11) NOT NULL,
  `nama_prov` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id_prov`, `kode_prov`, `nama_prov`) VALUES
(11, 33, 'Jawa Tengah'),
(27, 34, 'Jogjakarta');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('SuperAdmin','Admin','User') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `level`) VALUES
(1, 'Maorin', 'maorincarica@gmail.com', '$2y$10$peQqVSsz.BlSvfhvRw4cpOpg3GBAvzfhMvpeMU1RNNIZ6wgl.7CCi', 'SuperAdmin'),
(2, 'User', 'user@maorin.com', '$2y$10$ugXPldJuYw8n4vCuDESuRe4JkDdwRu.bL29lqNnzcxHFpMuU5fRIi', 'User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bbaku`
--
ALTER TABLE `bbaku`
  ADD PRIMARY KEY (`idBaku`);

--
-- Indexes for table `bjadi`
--
ALTER TABLE `bjadi`
  ADD PRIMARY KEY (`idBarang`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_pegawai`),
  ADD UNIQUE KEY `nama` (`nama`);

--
-- Indexes for table `katpelanggan`
--
ALTER TABLE `katpelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`subcategory_id`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id_prov`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bbaku`
--
ALTER TABLE `bbaku`
  MODIFY `idBaku` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bjadi`
--
ALTER TABLE `bjadi`
  MODIFY `idBarang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `katpelanggan`
--
ALTER TABLE `katpelanggan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id_kota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `subcategory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id_prov` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
