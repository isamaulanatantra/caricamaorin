<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav" id="sideUser">
                    <a class="nav-link" href="#">
                        <div class="sb-nav-link-icon"><i class="fas fa-desktop"></i></div>
                        Beranda
                    </a>
                    <a class="nav-link" href="<?= base_url('laporan'); ?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-chart-line"></i></div>
                        Laporan
                    </a>
                    <!-- <div class="sb-sidenav-menu-heading">-----------</div> -->
                    <a class="nav-link" href="#">
                        <div class="sb-nav-link-icon"><i class="fas fa-book"></i></div>
                        Buku Besar
                    </a>
                    <a class="nav-link" href="<?= base_url('penjualan') ?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-shopping-cart"></i></div>
                        Penjualan
                    </a>
                    <a class="nav-link" href="#">
                        <div class="sb-nav-link-icon"><i class="fas fa-shopping-bag"></i></div>
                        Pembelian
                    </a>
                    <a class="nav-link" href="#">
                        <div class="sb-nav-link-icon"><i class="fas fa-truck"></i></div>
                        Pengiriman
                    </a>
                    <a class="nav-link" href="#">
                        <div class="sb-nav-link-icon"><i class="fas fa-coins"></i></div>
                        Biaya
                    </a>
                    <!-- <div class="sb-sidenav-menu-heading">-----------</div> -->
                    <a class="nav-link" href="#">
                        <div class="sb-nav-link-icon"><i class="fas fa-utensils"></i></div>
                        Produksi
                    </a>
                    <a class="nav-link" href="#">
                        <div class="sb-nav-link-icon"><i class="fas fa-warehouse"></i></div>
                        Stok Gudang
                    </a>
                    <a class="nav-link" href="#">
                        <div class="sb-nav-link-icon"><i class="fas fa-hand-holding-usd"></i></div>
                        Asset
                    </a>
                    <!-- <div class="sb-sidenav-menu-heading">----------</div> -->
                    <a class="nav-link" href="<?= base_url('karyawan') ?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                        Karyawan
                    </a>
                    <a class="nav-link" href="<?= base_url('pelanggan') ?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                        Pelanggan
                    </a>
                    <a class="nav-link" href="<?= base_url('pengaturan'); ?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-cog"></i></div>
                        Pengaturan
                    </a>

                </div>
            </div>

        </nav>
    </div>