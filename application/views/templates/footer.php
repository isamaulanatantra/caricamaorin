<script src="<?= base_url('assets/') ?>js/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/scripts.js"></script>
<script src="<?= base_url('assets/') ?>js/Chart.min.js" crossorigin="anonymous"></script>
<!-- SweetAlert -->
<script src="<?= base_url('assets/'); ?>js/sweetalert2.all.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/myscript.js"></script>
<!-- DataTable -->
<script src="<?= base_url('assets/'); ?>js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/'); ?>js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/'); ?>assets/demo/datatables-demo.js"></script>
<script>
    <?php if ($this->session->userdata('level') == "Admin") { ?>

        $(document).ready(function() {

            $(".btn-danger").remove();

        });

    <?php } else if ($this->session->userdata('level') == "User") { ?>

        $(document).ready(function() {

            $("#sideUser").remove();

            // $(".td-btn").remove();

        });

    <?php } else {
    }; ?>
</script>
</body>

</html>