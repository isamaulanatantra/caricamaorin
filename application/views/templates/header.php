<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
   <meta name="description" content="Carica Terlaris di Wonsobo, Carica Maorin">
    <meta name="author" content="Carica Maorin">
    <title><?= $judul; ?></title>
    <link rel="shortcut icon" href="<?= base_url(); ?>ico.png">
    <link href="<?= base_url('assets/') ?>css/styles.css" rel="stylesheet" />
    <link href="<?= base_url('assets/') ?>css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="<?= base_url('assets/') ?>js/all.min.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?= base_url(); ?>assets/leaflet/leaflet.css" />
    <script src="<?= base_url(); ?>assets/leaflet/leaflet.js"></script>
    <link rel="stylesheet" href="<?= base_url('assets/') ?>dist/leaflet-routing-machine.css" />
    <script src="<?= base_url('assets/') ?>dist/leaflet-routing-machine.js"></script>

    <link rel="stylesheet" href="<?= base_url(); ?>assets/leaflet-locatecontrol/dist/L.Control.Locate.min.css" />
    <script src="<?= base_url(); ?>assets/leaflet-locatecontrol/src/L.Control.Locate.js"></script>
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/jquery-ui.css" />
</head>