<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="col-4 mt-2">
                <form method="POST">
                    <div class="form-group">
                        <label>Kode</label>
                        <input type="hidden" name="id_pegawai" value="<?= $agenda['id']; ?>">
                        <input type="text" id="provinsi" name="provinsi" class="form-control" value="<?= $agenda['kode']; ?>" onkeyup="kapitalAll()" autofocus required>
                        <small class="form-text text-danger"><?= form_error('provinsi'); ?></small>
                    </div>
                    <div class="form-group">
                        <label>Kategori</label>
                        <input type="text" id="kode_idpel" name=" kode_idpel" class="form-control" value="<?= $agenda['kategori']; ?>" onkeyup="kapitalKata()" required>
                        <small class="form-text text-danger"><?= form_error('kode_idpel'); ?></small>
                    </div>
                    <div class="form-group">
                        <label>Kisaran Harga</label>
                        <div class="input-group">
                            <input type="number" placeholder=" Harga Min" id="min" name="min" min="0" class="form-control" value="<?= $agenda['min']; ?>">
                            <input type="number" placeholder=" Harga Maks" id="max" name="max" min="0" onkeyup="cekH()" class="form-control" value="<?= $agenda['max']; ?>">
                        </div>
                        <small id="errorH" class="form-text text-danger"></small>
                        <small class="form-text text-danger"><?= form_error('max'); ?></small>
                    </div>
                    <div style="text-align: center;">
                        <a type="submit" href="<?= base_url('pengaturan/pelangganKategori') ?>" class="btn btn-secondary">Kembali</a>
                        <button type="submit" id="add_data" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous">
        </script>
        <script>
            function kapitalKata() {
                var t = document.getElementById("kode_idpel");
                t.value = Capitalize(t.value);
            }

            function Capitalize(str) {
                return str.replace(/\w\S*/g,
                    function(txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
            }

            function kapitalAll() {
                var x = document.getElementById("provinsi");
                x.value = x.value.toUpperCase();
            }

            function cekH() {
                if (parseInt(document.getElementById('min').value) >= parseInt(document.getElementById('max').value)) {
                    document.getElementById("errorH").innerHTML = "Inputan Maks Harus Lebih Besar!";
                    document.getElementById("add_data").setAttribute("disabled", "disabled");
                    return;
                } else {
                    document.getElementById("errorH").innerHTML = "";
                    document.getElementById("add_data").removeAttribute("disabled");
                    return;
                }
            }

            (function() {
                $(document.getElementsByTagName('input')).keyup(function() {
                    var empty = false;
                    $(document.getElementsByTagName('input')).each(function() {
                        if ($(this).val() == '') {
                            empty = true;
                        }
                    });

                    if (empty) {
                        $('#add_data').attr('disabled', 'disabled');
                    } else {
                        // $('#add_data').removeAttr('disabled');
                    }
                });
            })()
        </script>

    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>