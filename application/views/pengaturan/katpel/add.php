<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="col my-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>TAMBAH DATA KATEGORI PELANGGAN</b></div>
            <div class="col-4 mt-2">
                <form method="POST">
                    <div class="form-group">
                        <label>Kode</label>
                        <input type="text" id="provinsi" name="provinsi" class="form-control" value="<?= set_value('provinsi'); ?>" onkeyup="kapitalAll()" autofocus required>
                        <small class="form-text text-danger"><?= form_error('provinsi'); ?></small>
                    </div>
                    <div class="form-group">
                        <label>Kategori</label>
                        <input type="text" id="kode_idpel" name="kode_idpel" class="form-control" value="<?= set_value('kode_idpel'); ?>" onkeyup="kapitalKata()" required>
                        <small class="form-text text-danger"><?= form_error('kode_idpel'); ?></small>
                    </div>
                    <div class="form-group">
                        <label>Kisaran Harga</label>
                        <div class="input-group">
                            <input type="number" placeholder="Harga Min" id="min" name="min" min="0" class="form-control" value="<?= set_value('min'); ?>">
                            <input type="number" placeholder="Harga Maks" id="max" name="max" min="0" class="form-control" onkeyup="cekH()" value="<?= set_value('max'); ?>">
                        </div>
                        <small id="errorH" class="form-text text-danger"></small>
                        <small class="form-text text-danger"><?= form_error('max'); ?></small>
                    </div>
                    <div style="text-align: center;">
                        <a type="submit" href="<?= base_url('pengaturan/pelangganKategori') ?>" class="btn btn-secondary">Kembali</a>
                        <button type="submit" id="add_data" class="btn btn-primary" disabled="disabled">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous">
        </script>
        <script>
            function kapitalAll() {
                var x = document.getElementById("provinsi");
                x.value = x.value.toUpperCase();
            }

            function kapitalKata() {
                var t = document.getElementById("kode_idpel");
                t.value = Capitalize(t.value);
            }

            function Capitalize(str) {
                return str.replace(/\w\S*/g,
                    function(txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
            }

            function cekH() {
                if (parseInt(document.getElementById('min').value) >= parseInt(document.getElementById('max').value)) {
                    document.getElementById("errorH").innerHTML = "Inputan Maks Harus Lebih Besar!";
                    document.getElementById("add_data").setAttribute("disabled", "disabled");
                    return;
                } else {
                    document.getElementById("errorH").innerHTML = "";
                    document.getElementById("add_data").removeAttribute("disabled");
                    return;
                }
            }

            (function() {
                $(document.getElementsByTagName('input')).keyup(function() {
                    var empty = false;
                    $(document.getElementsByTagName('input')).each(function() {
                        if ($(this).val() == '') {
                            empty = true;
                        }
                    });

                    if (empty) {
                        $('#add_data').attr('disabled', 'disabled');
                    } else {
                        // $('#add_data').removeAttr('disabled');
                    }
                });
            })()
        </script>

    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>