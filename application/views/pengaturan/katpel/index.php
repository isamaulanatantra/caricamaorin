<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- Breadcrumbs-->
                    <ol class="breadcrumb mt-2">
                        <?php foreach ($this->uri->segments as $segment) : ?>
                            <?php
                            $url = substr($this->uri->uri_string, 0, strpos($this->uri->uri_string, $segment)) . $segment;
                            $is_active =  $url == $this->uri->uri_string;
                            ?>
                            <li class="breadcrumb-item <?php echo $is_active ? 'active' : '' ?>">
                                <?php if ($is_active) : ?>
                                    <!-- <?php echo ucfirst($segment) ?> -->
                                    <?= ucfirst("Pelanggan Kategori") ?>
                                <?php else : ?>
                                    <a href="<?php echo site_url($url) ?>"><?php echo ucfirst($segment) ?></a>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ol>
                    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
                    <?php if ($this->session->flashdata('flash')) : ?>
                    <?php endif; ?>
                    <div class="col mb-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>DATA KATEGORI PELANGGAN</b></div>
                    <a href="<?= base_url('pengaturan/addKatpel'); ?>" class="btn btn-primary mb-2">Tambah Data</a>
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                                    <thead style="font-size: small; text-align: center;">
                                        <tr>
                                            <th>No</th>
                                            <th>Kode</th>
                                            <th>Kategori</th>
                                            <th>Kisaran Harga</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: small;">
                                        <?php
                                        $no = 1;
                                        foreach ($kategori as $buku) {
                                        ?>
                                            <tr style="text-align: center;">
                                                <td><?= $no++; ?></td>
                                                <td><?= $buku->kode; ?></td>
                                                <td><?= $buku->kategori; ?></td>
                                                <td><?= 'Rp ' . number_format($buku->min, 0, ".", ".") . ' s/d Rp. ' . number_format($buku->max, 0, ".", ".") . '' ?> </td>
                                                <td align="center">
                                                    <a href="<?= base_url();  ?>pengaturan/ubahKatpel/<?= $buku->id; ?>"><i class="fas fa-fw fa-edit"></i></a>
                                                    <a href="<?= base_url();  ?>pengaturan/hapusKatpel/<?= $buku->id; ?>" class="tombol-hapus"><i class="fas fa-fw fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End of Main Content -->
                </div>
            </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>