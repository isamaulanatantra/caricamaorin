<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="tr">
                <div class="col">
                    <!-- <div class="col" style="text-align: center; color: white; background-color: #3d75e3;"><b>PENGATURAN</b></div> -->
                    <div class="col my-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>PENGATURAN</b></div>
                </div>
            </div>
            <div class="tr">
                <div class="col-3">
                    <!-- <div id="layoutSidenav_nav"> -->
                    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                        <div class="sb-sidenav-menu">
                            <div class="nav">
                                <a class="nav-link" href="#">
                                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                    Karyawan
                                </a>
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                    Pelanggan
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <nav class="sb-sidenav-menu-nested nav">
                                        <a class="nav-link" href="<?= base_url('pengaturan/pelangganKategori') ?>">Kategori</a>
                                        <a class="nav-link" href="<?= base_url('pengaturan/provinsi') ?>">Provinsi</a>
                                        <a class="nav-link" href="<?= base_url('pengaturan/kota') ?>">Kota</a>
                                    </nav>
                                </div>
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                    Produksi
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                        <a class="nav-link" href="<?= base_url('pengaturan/kategori') ?>">
                                            Kategori
                                        </a>

                                        <a class="nav-link" href="<?= base_url('pengaturan/bBaku') ?>">
                                            Bahan Baku
                                        </a>
                                        <!-- <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                            <nav class="sb-sidenav-menu-nested nav">
                                                <a class="nav-link" href="#">401 Page</a>
                                                <a class="nav-link" href="#">404 Page</a>
                                                <a class="nav-link" href="#">500 Page</a>
                                            </nav>
                                        </div> -->

                                        <a class="nav-link collapsed" href="<?= base_url('pengaturan/produk') ?>" aria-expanded="false" aria-controls="pagesProduksi">
                                            Produk
                                            <!-- <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div> -->
                                        </a>
                                        <!-- <div class="collapse" id="pagesProduksi" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                            <nav class="sb-sidenav-menu-nested nav">
                                                <a class="nav-link" href="#">401 Page</a>
                                                <a class="nav-link" href="#">404 Page</a>
                                                <a class="nav-link" href="#">500 Page</a>
                                            </nav>
                                        </div> -->
                                    </nav>
                                </div>
                                <a class="nav-link" href="#">
                                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                    Stok
                                </a>
                            </div>
                        </div>

                    </nav>
                    <!-- </div> -->
                </div>
            </div>

        </div>

    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>