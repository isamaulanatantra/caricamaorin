<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="col my-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>TAMBAH DATA KOTA</b></div>
            <div class="col-3 mt-2">
                <form method="POST">
                    <div class="form-group">
                        <label>Provinsi</label>
                        <!-- <input type="text" id="id_prov" name="id_prov" class="form-control" value="<?= set_value('id_prov') ?>" hidden> -->
                        <input type="text" id="provinsi" name="provinsi" class="form-control" value="<?= set_value('provinsi'); ?>" autofocus>
                        <small id="username_result" class="form-text text-danger"><?= form_error('provinsi'); ?></small>
                    </div>
                    <div class="form-group">
                        <label>Kode Pelanggan</label>
                        <input type="text" id="kode_idpel" name="kode_idpel" class="form-control isi" value="<?= set_value('kode_idpel'); ?>" onkeyup="kapitalAll()" required>
                        <small class="form-text text-danger"><?= form_error('kode_idpel'); ?></small>
                    </div>
                    <div class="form-group">
                        <label>Nama Kota / Kab</label>
                        <input type="text" id="nama_kota" name="nama_kota" class="form-control isi" onkeyup="kapitalKata()" value="<?= set_value('nama_kota'); ?>" required>
                        <small class="form-text text-danger"><?= form_error('nama_kota'); ?></small>
                    </div>
                    <div style="text-align: center;">
                        <a type="submit" href="<?= base_url('pengaturan/kota') ?>" class="btn btn-secondary">Kembali</a>
                        <button id="add_data" type="submit" disabled="disabled" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous">
        </script>
        <script>
            function kapitalKata() {
                var t = document.getElementById("nama_kota");
                t.value = Capitalize(t.value);
            }

            function Capitalize(str) {
                return str.replace(/\w\S*/g,
                    function(txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
            }

            function kapitalAll() {
                var x = document.getElementById("kode_idpel");
                x.value = x.value.toUpperCase();
            }

            $("#provinsi").autocomplete({
                source: "<?= site_url('pengaturan/autoProv/?'); ?>",

                select: function(event, ui) {
                    $('[name="id_prov"]').val(ui.item.description2);
                    $('[name="provinsi"]').val(ui.item.label);
                }
            });

            $(document).change(function() {
                $(document.getElementsByTagName('input')).keyup(function() {
                    var empty = false;
                    var d = document.getElementById('username_result').innerText;
                    $(document.getElementsByTagName('input')).each(function() {
                        if ($(this).val() == '') {
                            empty = true;
                        }
                    });
                    // console.log(empty);
                    // console.log(d);
                    if (empty == false && d != 'Provinsi belum terdaftar!') {
                        $('#add_data').removeAttr('disabled');
                    } else {
                        $('#add_data').attr('disabled', 'disabled');
                    }
                });
            });

            $(document).ready(function() {
                $('#provinsi').change(function() {
                    var provinsi = $('#provinsi').val();
                    if (provinsi != '') {
                        $.ajax({
                            url: "<?= base_url(); ?>Search/checkUsername",
                            method: "POST",
                            data: {
                                provinsi: provinsi
                            },
                            success: function(data) {
                                $('#username_result').html(data);
                            }
                        });
                    }
                });
            });
        </script>

    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>