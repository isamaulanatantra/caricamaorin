<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row mt-3">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <!-- <h1 class="h3 mb-4 text-gray-800"><?= $judul; ?></h1> -->
                            Form Ubah Data Kota
                        </div>
                        <div class="card-body">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input type="hidden" name="id_pegawai" value="<?= $agenda['id_kota']; ?>">
                                            <label for="nama">Kode Kota</label>
                                            <input type="text" name="idpel" class="form-control" id="idpel" value="<?= $agenda['kode_idpel']; ?>" required>
                                            <small class="form-text text-danger"><?= form_error('idpel'); ?></small>
                                            <label for="alamat">Nama Kota / Kab</label>
                                            <input type="text" name="kotakab" class="form-control" id="kotakab" value="<?= $agenda['nama_kota']; ?>" required>
                                            <small class="form-text text-danger"><?= form_error('kotakab'); ?></small>

                                            <button type="submit" name="ubah" class="btn btn-primary float-right">Ubah Data</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- </div> -->