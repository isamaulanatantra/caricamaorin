<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="col my-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>TAMBAH DATA PROVINSI</b></div>
            <div class="col-3 mt-2">
                <form method="POST">
                    <div class="form-group">
                        <label>Kode Provinsi</label>
                        <input type="number" min="0" name="kodeProv" class="form-control" value="<?= set_value('kodeProv'); ?>" autofocus required>
                        <small class="form-text text-danger"><?= form_error('kodeProv'); ?></small>
                    </div>
                    <div class="form-group">
                        <label>Nama Provinsi</label>
                        <input type="text" id="namaProv" name="namaProv" class="form-control" value="<?= set_value('namaProv'); ?>" onkeyup="kapitalKata()" required>
                        <small class="form-text text-danger"><?= form_error('namaProv'); ?></small>
                    </div>
                    <div style="text-align: center;">
                        <a type="submit" href="<?= base_url('pengaturan/provinsi') ?>" class="btn btn-secondary">Kembali</a>
                        <button id="add_data" disabled="disabled" type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>
            function kapitalKata() {
                var t = document.getElementById("namaProv");
                t.value = Capitalize(t.value);
            }

            function Capitalize(str) {
                return str.replace(/\w\S*/g,
                    function(txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
            }
            (function() {
                $(document.getElementsByTagName('input')).keyup(function() {
                    var empty = false;
                    $(document.getElementsByTagName('input')).each(function() {
                        if ($(this).val() == '') {
                            empty = true;
                        }
                    });

                    if (empty) {
                        $('#add_data').attr('disabled', 'disabled');
                    } else {
                        $('#add_data').removeAttr('disabled');
                    }
                });
            })()
        </script>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>