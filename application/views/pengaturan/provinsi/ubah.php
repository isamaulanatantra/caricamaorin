<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="col-3 mt-2">
                <form method="POST">
                    <div class="form-group">
                        <label>Kode Provinsi</label>
                        <input type="hidden" name="id" value="<?= $agenda['id_prov']; ?>">
                        <input type="number" value="<?= $agenda['kode_prov']; ?>" min="0" name="kodeProv" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Nama Provinsi</label>
                        <input type="text" value="<?= $agenda['nama_prov']; ?>" name="namaProv" class="form-control" required>
                    </div>
                    <div style="text-align: center;">
                        <a type="submit" href="<?= base_url('pengaturan/provinsi') ?>" class="btn btn-secondary">Kembali</a>
                        <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                </form>
            </div>
        </div>

    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>