<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Carica Terlaris di Wonsobo, Carica Maorin">
    <meta name="author" content="Carica Maorin">
    <title>Carica Maorin</title>
    <link rel="shortcut icon" href="<?= base_url(); ?>ico.png">
    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('assets'); ?>/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?= base_url('assets'); ?>/css/signin.css" rel="stylesheet">
</head>

<body class="text-center">
    <form class="form-signin" action="<?= base_url('index.php/auth/loginForm') ?>" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Silahkan Masuk</h1>
        <?php
        $errors = $this->session->flashdata('errors');
        if (!empty($errors)) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger text-center">
                        <?php foreach ($errors as $key => $error) { ?>
                            <?php echo "$error<br>"; ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php
        }
        if ($msg = $this->session->flashdata('error_login')) { ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger text-center">
                        <?= $msg ?>
                    </div>
                </div>
            </div>
        <?php } else if ($msg = $this->session->flashdata('success_login')) { ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success text-center">
                        <?= $msg ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Alamat Email" required autofocus> <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Kata Sandi" required>
        <div class="checkbox mb-3" hidden>
            <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
        <a href="<?= base_url('index.php/auth/register') ?>" class="float-left mt-1" hidden>Register</a>
        <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
    </form>
</body>

</html>