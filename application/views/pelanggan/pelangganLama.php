<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <!-- Begin Page Content -->
            <!-- Page Heading -->
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <div style="color: red;"><?= (isset($message)) ? $message : ""; ?></div>

            <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
            <?php if ($this->session->flashdata('flash')) : ?>
            <?php endif; ?>
            <!-- <?= var_dump($agenda) ?> -->
            <span id="norutu" hidden><?= $nomorUrut['noUrut'] ?></span>
            <!-- <span id="kotah" hidden><?= $agenda['subcategory_category_id'] ?></span> -->
            <span id="latDB" hidden><?= $agenda['latitude'] ?></span>
            <span id="longDB" hidden><?= $agenda['longitude'] ?></span>
            <?= form_open("pelanggan/tambahFoto", array('enctype' => 'multipart/form-data')); ?>
            <div class="col mt-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>INPUT DATA PELANGGAN</b></div>
            <div class="col-3 mt-1" style="float: right;">
                <div class="card">
                    <div class="imgWrap">
                        <img src="<?= base_url(); ?>assets/img/no-image.png" id="imgView" class="w-100">
                    </div>
                    <div class="card-body">
                        <div class="custom-file">
                            <input type="file" id="inputFile" name="inputFile" class="imgFile custom-file-input" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputFile">Choose file</label>
                        </div>
                    </div>
                </div>

                <div class="mt-2" id="map" style="height: 250px;"></div>

                <div class="mt-2" hidden>
                    <input placeholder="Latitude" class="form-control" type="text" id="Latitude" name="lat" value="<?= set_value('lat'); ?>" readonly>
                    <input placeholder="Longitude" class="form-control" type="text" id="Longitude" name="long" value="<?= set_value('long'); ?>" readonly>
                </div>

            </div>

            <div class="row">
                <div class="col mt-">Kategori * :
                    <label class="radio-inline mr-3">
                        <input id="coba1" class="form-control-input" type="radio" name="kategori" value="Distributor" <?= ($agenda['kategori'] == 'Distributor' ? ' checked' : ''); ?> required> Distributor
                    </label>
                    <label class="radio-inline mr-3">
                        <input id="coba2" class="form-control-input" type="radio" name="kategori" value="Agen" <?= ($agenda['kategori'] == 'Agen' ? ' checked' : ''); ?>> Agen
                    </label>
                    <label class="radio-inline">
                        <input id="coba3" class="form-control-input" type="radio" name="kategori" value="Konsumen" <?= ($agenda['kategori'] == 'Konsumen' ? ' checked' : ''); ?>> Konsumen
                    </label>
                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    <div class="input-group-sm">
                        <input placeholder="ID Pelanggan *" maxlength="10" class="form-control" type="text" id="idPelanggan" name="idPelanggan" value="<?= set_value('idPelanggan'); ?>" readonly>
                    </div>
                </div>
                <div class="col-4">
                    <div class="input-group-sm">
                        <!-- <input type="text" class="form-control" name="nKota" id="nKota" placeholder="Kota / Kab * " value="<?= $agenda['subcategory_category_id']; ?>" required> -->
                        <input type="text" class="form-control" name="nKota" id="nKota" placeholder="Kota / Kab * " required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="input-group-sm">
                        <input placeholder="Email Pelanggan" class="form-control" type="email" onkeyup="lowerAll()" id="email" name="email" value="<?= $agenda['email']; ?>">
                        <small id="emailE" class="form-text text-danger"></small>
                    </div>
                </div>
            </div>
            <div class="row mt-1">
                <div class="col-6">
                    <div class="input-group-sm">
                        <input placeholder="Nama Pelanggan *" class="form-control" onkeyup="kapitalKata()" type="text" id="nama" name="nama" value="<?= $agenda['subcategory_name']; ?>" required>
                    </div>
                </div>
                <div class="col-6">
                    <div class="input-group-sm">
                        <input placeholder="Nama Personal" class="form-control" type="text" id="namper" onkeyup="kapitalKata()" name="namaPersonal" value="<?= $agenda['namaPersonal']; ?>">
                    </div>
                </div>
            </div>

            <div class="row mt-1">
                <div class="input-group-sm col-6">
                    <input placeholder="Telp / HP" class="form-control" type="text" name="telp" value="<?= $agenda['telp'] ?>">
                </div>
                <div class="input-group-sm col-6">
                    <input placeholder="No. Whatsapp" class="form-control" type="text" name="waa" value="<?= $agenda['nowa'] ?>">
                </div>
            </div>
            <div class="row mt-1">
                <div class="col-9">
                    <div class="input-group-sm">
                        <input placeholder="Alamat Pelanggan" class="form-control" type="text" onkeyup="kapitalKata()" id="alamat" name="alamat" value="<?= $agenda['alamat_toko']; ?>">
                    </div>
                </div>
                <div class="col-3">
                    <div class="input-group-sm">
                        <input type="date" class="form-control" name="tanggal" required>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-2" style="color: white; text-align: center; background-color: #0275d8;"><b>PRODUK</b></div>
            </div>

            <div class="row addddd">
                <div class="table-responsive mt-2">
                    <table class="table table-striped" width="100%" cellspacing="0">
                        <tr style="text-align: center;">
                            <th>
                                Nama Produk
                            </th>
                            <th>
                                Satuan
                            </th>
                            <th>
                                Harga
                            </th>
                            <th>
                                Banyaknya
                            </th>
                            <th>
                                Jumlah
                            </th>
                            <th>
                                Aksi
                            </th>
                        </tr>
                        <Tbody id="tabelBarang"></Tbody>
                        <tr class="inputProduk">
                            <th>
                                <div class="input-group-sm">
                                    <select name="pilihProduk" id="pilihProduk" class="form-control">
                                        <option value="">Produk</option>
                                        <?php foreach ($produk as $nama1) : ?>

                                            <option value="<?= $nama1['namaBarang']; ?>" <?= (set_value('pilihProduk') == $nama1['namaBarang'] ? ' selected' : ''); ?>>
                                                <?= $nama1['namaBarang']; ?>
                                            </option>

                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </th>
                            <th>
                                <div class="input-group-sm">
                                    <select class="form-control" id="satuan">
                                        <option>PCS</option>
                                    </select>
                                </div>
                            </th>
                            <th>
                                <div class="input-group-sm harga">
                                    <input type="number" class="form-control" id="harga" min="0">
                                </div>
                            </th>
                            <th>
                                <div class="input-group-sm jumlah">
                                    <input type="number" class="form-control" id="jumlah" min="0">
                                </div>
                            </th>
                            <th></th>
                            <th style="text-align: center;">
                                <a class="add-more" type="button"><i class="fas fa-plus-circle"></i></a>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="6">
                                <div style="text-align: right;">
                                    <span>TOTAL = </span>
                                    <span id="totalHarga">0</span>
                                </div>
                            </th>
                        </tr>
                    </table>
                </div>

            </div>

            <div class="row mt-2">

                <div class="col">
                    <center>
                        <a href="<?= base_url('pelanggan'); ?>"><input class="mr-1 btn btn-secondary r-3" type="button" value="Kembali"></a>
                        <input type="submit" name="submit" value="Simpan" class="btn btn-primary">
                    </center>
                </div>
            </div>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
            <script>
                function lowerAll() {
                    var x = document.getElementById("email");
                    x.value = x.value.toLowerCase();
                    // var rs = document.getElementById('email').value;
                    var rsi = document.getElementById('emailE');
                    // console.log(rs);
                    var atps = x.value.indexOf("@");
                    var dots = x.value.lastIndexOf(".");
                    if (atps < 1 || dots < atps + 2 || dots + 2 >= x.value.length) {
                        // alert("Alamat email tidak valid.");
                        rsi.innerText = "Alamat email tidak benar!";
                        return false;
                    } else {
                        rsi.innerText = '';
                        // alert("Alamat email valid.");
                    }
                }

                function kapitalKata() {
                    var t = document.getElementById("nama");
                    var y = document.getElementById("alamat");
                    var z = document.getElementById("namper");
                    t.value = Capitalize(t.value);
                    y.value = Capitalize(y.value);
                    z.value = Capitalize(z.value);
                }

                function Capitalize(str) {
                    return str.replace(/\w\S*/g,
                        function(txt) {
                            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        });
                }

                navigator.geolocation.getCurrentPosition(function(location) {
                    var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
                    // var curLocation = [0, 0];
                    var curLocation = [<?= $agenda['latitude'] ?>, <?= $agenda['longitude'] ?>];
                    // if (curLocation[0] == 0 && curLocation[1] == 0) {
                    //     curLocation = [location.coords.latitude, location.coords.longitude];
                    // }
                    let late = document.getElementById('latDB').innerText;
                    let longe = document.getElementById('longDB').innerText;
                    // var map = L.map('map').setView([location.coords.latitude, location.coords.longitude], 11);
                    var map = L.map('map').setView([parseFloat(late), parseFloat(longe)], 11);
                    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                        // attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                        id: 'mapbox/streets-v11'
                    }).addTo(map);

                    map.attributionControl.setPrefix(false);

                    var marker = new L.marker(curLocation, {
                        draggable: 'true'

                    });
                    $("#Latitude").val(location.coords.latitude),
                        $("#Longitude").val(location.coords.longitude),

                        marker.on('dragend', function(event) {
                            var position = marker.getLatLng();
                            marker.setLatLng(position, {
                                draggable: 'true'
                            }).bindPopup(position).update();
                            $("#Latitude").val(position.lat);
                            $("#Longitude").val(position.lng).keyup();
                        });

                    $("#Latitude, #Longitude").change(function() {
                        var position = [parseInt($("#Latitude").val()), parseInt($("#Longitude").val())];
                        marker.setLatLng(position, {
                            draggable: 'true'
                        }).bindPopup(position).update();
                        map.panTo(position);
                    });

                    map.addLayer(marker);
                });


                $("#inputFile").change(function(event) {
                    fadeInAdd();
                    getURL(this);
                });

                $("#inputFile").on('click', function(event) {
                    fadeInAdd();
                });

                function getURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        var filename = $("#inputFile").val();
                        filename = filename.substring(filename.lastIndexOf('\\') + 1);
                        reader.onload = function(e) {
                            debugger;
                            $('#imgView').attr('src', e.target.result);
                            $('#imgView').hide();
                            $('#imgView').fadeIn(500);
                            $('.custom-file-label').text(filename);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    $(".alert").removeClass("loadAnimate").hide();
                }

                function fadeInAdd() {
                    fadeInAlert();
                }

                function fadeInAlert(text) {
                    $(".alert").text(text).addClass("loadAnimate");
                }


                $("#nKota").autocomplete({
                    source: "<?= site_url('pelanggan/get_autocomplete/?'); ?>",

                    select: function(event, ui) {
                        var ab = $('#idPelanggan').val();
                        var r = ab.substring(0, 1);
                        var t = ab.substring(1, 6);
                        $('[name="nKota"]').val(ui.item.label);
                        $('[name="idPelanggan"]').val(r + ui.item.description2 + t);
                    }
                });

                $(document).change(function() {
                    if ($('#email').val() == '') {
                        document.getElementById('emailE').innerText = '';
                    }
                    let ipel = document.getElementById('idPelanggan').value;
                    let norut2 = document.getElementById('norutu').innerHTML;
                    if (norut2.length == 1) {
                        norut2 = parseInt(norut2) + 1;
                        norut2 = '000' + norut2;
                    } else if (norut2.length == 2) {
                        norut2 = parseInt(norut2) + 1;
                        norut2 = '00' + norut2;
                    } else if (norut2.length == 3) {
                        norut2 = parseInt(norut2) + 1;
                        norut2 = '0' + norut2;
                    } else if (norut2.length == 4) {
                        norut2 = parseInt(norut2) + 1;
                        norut2 = '' + norut2;
                    }

                    if (ipel.length == 2) {
                        $("#idPelanggan").val(ipel + norut2);
                    } else if (ipel.length >= 7) {
                        var l = ipel.substring(0, 3);
                        $("#idPelanggan").val(l + norut2);
                    }

                    autoSum();
                    radiobtn();
                });

                function radiobtn() {
                    // $("input[type='radio']").change(function() {
                    let norut = document.getElementById('norutu').innerHTML;

                    var radioValue = $("input[name='kategori']:checked").val();
                    var idpel = document.getElementById('idPelanggan').value;
                    if (idpel == '') {
                        if (norut.length == 1) {
                            norut = parseInt(norut) + 1;
                            norut = '000' + norut;
                        } else if (norut.length == 2) {
                            norut = parseInt(norut) + 1;
                            norut = '00' + norut;
                        } else if (norut.length == 3) {
                            norut = parseInt(norut) + 1;
                            norut = '0' + norut;
                        } else if (norut.length == 4) {
                            norut = parseInt(norut) + 1;
                            norut = '' + norut;
                            console.log(norut);
                        }
                        var potong = radioValue.substring(0, 1);
                        // $("#idPelanggan").val(potong + makeid(5));
                        $("#idPelanggan").val(potong + norut);
                    } else if (idpel.length == 5) {
                        var u = radioValue.substring(0, 1);
                        var i = idpel.substring(1, 5);
                        $("#idPelanggan").val(u + i);
                    } else if (idpel.length == 6) {
                        var s = radioValue.substring(0, 1);
                        $("#idPelanggan").val(s + idpel);
                    } else if (idpel.length == 7) {
                        var s = radioValue.substring(0, 1);
                        var l = idpel.substring(1, 7);
                        $("#idPelanggan").val(s + l);
                    }
                    // });
                }

                $(document).ready(function() {
                    radiobtn();
                    // let kotah = document.getElementById('kotah').innerText;
                    // $("#nKota").autocomplete({
                    //     source: "<?= site_url('pelanggan/get_autocomplete/?wonosobo'); ?>",

                    //     select: function(event, ui) {
                    //         var ab = $('#idPelanggan').val();
                    //         var r = ab.substring(0, 1);
                    //         var t = ab.substring(1, 6);
                    //         $('[name="nKota"]').val(ui.item.label);
                    //         $('[name="idPelanggan"]').val(r + ui.item.description2 + t);
                    //     }
                    // });

                    $(".add-more").click(function() {
                        var id_pelanggan = $('#idPelanggan').val();
                        var id_bjadi = $('#pilihProduk').val();
                        var satuan = $('#satuan').val();
                        var harga = $('#harga').val();
                        var jumlah = $('#jumlah').val();
                        let kali = harga * jumlah;

                        // membuat format rupiah field Harga
                        var number_string = harga.toString(),
                            sisa = number_string.length % 3,
                            rupiah = 'Rp. ' + number_string.substr(0, sisa),
                            ribuan = number_string.substr(sisa).match(/\d{3}/g);

                        if (ribuan) {
                            separator = sisa ? '.' : '';
                            rupiah += separator + ribuan.join('.');
                        }

                        // membuat format rupiah field Jumlah
                        var number_string2 = kali.toString(),
                            sisa2 = number_string2.length % 3,
                            rupiah2 = 'Rp. ' + number_string2.substr(0, sisa2),
                            ribuan2 = number_string2.substr(sisa2).match(/\d{3}/g);

                        if (ribuan2) {
                            separator2 = sisa2 ? '.' : '';
                            rupiah2 += separator2 + ribuan2.join('.');
                        }

                        $.ajax({
                            type: "POST",
                            async: true,
                            data: {
                                id_pelanggan: id_pelanggan,
                                id_bjadi: id_bjadi,
                                satuan: satuan,
                                harga: harga,
                                jumlah: jumlah,

                            },
                            dataType: "text",
                            url: "<?= base_url('pelanggan/tambahBarang'); ?>",
                            success: function(json) {
                                var data = "";
                                if (json != "error") {
                                    data = "<tr><td id='" + json + "'>" + id_bjadi + "</td><td id='" + json + "' style='text-align: center;'>" + satuan + "</td><td id='" + json + "'>" + rupiah + "</td><td id='" + json + "'>" + jumlah + "</td><td  id='" + json + "' nowrap>" + rupiah2 + "</td><td id='" + json + "' style='text-align: center;'><a class='remove' type='button'><i class='fas fa-fw fa-minus-circle'></i></a><a class='edit' type='button'><i class='fas fa-fw fa-edit'></i></a></td></tr>";
                                    $('#tabelBarang').append(data);
                                    $('#harga').val('');
                                    $('#pilihProduk').val('');
                                    $('#jumlah').val('');

                                    autoSum();

                                }
                            }
                        })
                    });

                    // aksi tombol edit
                    $("#tabelBarang").on("click", '.edit', function(e) {
                        if (confirm('Yakin diubah?')) {
                            var id = $('td').attr('id');
                            $.ajax({
                                type: "POST",
                                async: true,
                                data: {
                                    id: id,
                                },
                                dataType: "text",
                                url: "<?= base_url('pelanggan/hapusBarang'); ?>",
                                success: function(json) {
                                    if (json != "error") {
                                        // $('[id=' + id + ']').remove();
                                        // $('#tabelBarang').append();
                                        e.currentTarget.parentElement.parentElement.remove();
                                        autoSum();
                                    }
                                }
                            })
                            $(this).parents(".control-group").remove();
                            $('#pilihProduk').val(e.currentTarget.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.innerText);

                            // hapus rupiah saat edit di klik
                            let xxx = e.currentTarget.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.innerText.toString();
                            editedit = xxx.replace('Rp. ', '');
                            editfix = editedit.replace(/\./g, '');
                            $('#harga').val(editfix);
                            $('#jumlah').val(e.currentTarget.parentElement.previousElementSibling.previousElementSibling.innerText);
                        }
                    });

                    // saat tombol remove dklik control group akan dihapus 
                    $("#tabelBarang").on("click", ".remove", function(e) {
                        if (confirm('Yakin dihapus?')) {
                            var id = $('td').attr('id');
                            $.ajax({
                                type: "POST",
                                async: true,
                                data: {
                                    id: id,
                                },
                                dataType: "text",
                                url: "<?= base_url('pelanggan/hapusBarang'); ?>",
                                success: function(json) {
                                    if (json != "error") {
                                        // $('[id=' + id + ']').remove();
                                        // $('#tabelBarang').append();
                                        e.currentTarget.parentElement.parentElement.remove();
                                        autoSum();
                                    }
                                }
                            })
                            // $(this).parents(".control-group").remove();
                            // e.currentTarget.parentElement.parentElement.remove();
                            // console.log(e.currentTarget.parentElement.parentElement);
                        }
                    });
                });

                function autoSum() {
                    // perkalian harga dan jumlah barang
                    var table = document.getElementById("tabelBarang");
                    sumHsl = 0;
                    for (var t = 0; t < table.rows.length; t++) {
                        let xxx = table.rows[t].cells[4].innerHTML;
                        harga1 = xxx.replace('Rp. ', '');
                        harga2 = harga1.replace(/\./g, '');
                        sumHsl = sumHsl + parseInt(harga2);
                    }

                    // membuat format rupiah field Sub Total
                    var number_string4 = sumHsl.toString(),
                        sisa4 = number_string4.length % 3,
                        rupiah4 = 'Rp. ' + number_string4.substr(0, sisa4),
                        ribuan4 = number_string4.substr(sisa4).match(/\d{3}/g);

                    if (ribuan4) {
                        separator4 = sisa4 ? '.' : '';
                        rupiah4 += separator4 + ribuan4.join('.');
                    }

                    document.getElementById("totalHarga").innerHTML = rupiah4;
                }
            </script>
            <?= form_close(); ?>
        </div>
        <!-- /.container-fluid -->
        <!-- </div> -->
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>