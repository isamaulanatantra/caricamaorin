<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
                    <?php if ($this->session->flashdata('flash')) : ?>
                    <?php endif; ?>

                    <div class="col my-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>DATA PELANGGAN</b></div>
                    <!-- <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newSubMenuModal">Tambah Data</a> -->
                    <a href="<?= base_url('pelanggan/addPelanggan'); ?>" class="btn btn-primary mb-2">Tambah Data</a>
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                                    <!-- <table class="table table-striped table-bordered" id="table_id" width="100%" cellspacing="0"> -->
                                    <thead style="font-size: small; text-align: center;">
                                        <tr>
                                            <th>No</th>
                                            <th>ID & Nama Pelanggan</th>
                                            <th>Kota & Alamat</th>
                                            <th>No. Telp / HP</th>
                                            <th>Produk</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: small;">
                                        <?php
                                        $no = 1;
                                        foreach ($join as $buku) {
                                        ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td><?= $buku->kodeToko; ?><br>
                                                    <a class="badge badge-success" href="" data-toggle="modal" data-target="#myModal" onclick="view('<?= $buku->foto ?>','<?= $buku->latitude; ?>','<?= $buku->longitude; ?>');">
                                                        <?= $buku->subcategory_name; ?>
                                                    </a>
                                                </td>
                                                <td NOWRAP>
                                                    <?= $buku->subcategory_category_id; ?><br>
                                                    <a href="#" id="btnRute" onclick="redirect('<?= $buku->latitude; ?>','<?= $buku->longitude; ?>');return false;" class="badge badge-warning">
                                                        <?= $buku->alamat_toko; ?>
                                                    </a>
                                                </td>
                                                <td><?= $buku->telp; ?><br>
                                                    <!-- <a href="https://api.whatsapp.com/send?phone=<?= $buku->nowa; ?>&text=Halo" target="_blank">
                                                        <button style="background:#32C03C;vertical-align:center;height:36px;border-radius:5px">
                                                            <img src="https://web.whatsapp.com/img/favicon/1x/favicon.png"> WA. <?= $buku->nowa; ?></button></a> -->
                                                </td>
                                                <td>
                                                    <?php
                                                    $this->db->select('*');
                                                    $this->db->where(['pembelian.id_pelanggan' => $buku->kodeToko]);
                                                    $this->db->from('pembelian');
                                                    // $this->db->join('bjadi', 'pembelian.id_bjadi = bjadi.namaBarang', 'left');
                                                    // $this->db->order_by('kodeToko', 'ASC');
                                                    $query = $this->db->get();
                                                    $result = $query->result();

                                                    foreach ($result as $row) {
                                                        echo '<table width="100%">';
                                                        echo '<tr>';
                                                        echo '<td width="100%">' . $row->id_bjadi . '</td>';
                                                        echo '<td style="text-align: left;" nowrap>Rp. ' . number_format($row->harga, 0, ".", ".") . '</td>';
                                                        echo '</tr>';
                                                        echo '</table>';
                                                    }
                                                    ?>
                                                </td>
                                                <td align="center">
                                                    <a href="<?= base_url();  ?>pelanggan/ubahPelanggan/<?= $buku->subcategory_id; ?>"><i class="fas fa-fw fa-info-circle"></i></a>
                                                    <a href="<?= base_url();  ?>pelanggan/pelangganLama/<?= $buku->subcategory_id; ?>"><i class="fas fa-fw fa-edit"></i></a>
                                                    <!-- <a href="<?= base_url();  ?>pelanggan/hapusPelanggan/<?= $buku->subcategory_id; ?>" class="tombol-hapus"><i class="fas fa-fw fa-trash-alt"></i></a> -->
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End of Main Content -->

                    <!-- Modal Photo-->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                </div>
                                <div class="modal-body" id="foto">
                                </div>
                                <div class="modal-footer" id="rute">
                                </div>
                            </div>
                        </div>
                    </div>

                    <script>
                    </script>
                    <script type="text/javascript">
                        function view(foto, latitude, longitude) {
                            $('#foto').html('<img src="<?= base_url(); ?>assets/upload/' + foto + '" class="w-100" alt="Responsive image">');
                        }
                    </script>
                    <script>
                        function redirect(latitude, longitude) {
                            navigator.geolocation.getCurrentPosition(function(location) {
                                var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
                                // console.log(location.coords.latitude, location.coords.longitude);
                                window.open("https://www.google.com/maps/dir/?api=1&origin=" + location.coords.latitude + "," + location.coords.longitude + "&destination=" + latitude + "," + longitude);
                            });
                        }
                    </script>
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>