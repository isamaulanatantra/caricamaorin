<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row mt-3">
                <div class="col-md">
                    <!-- <?= var_dump($join) ?> -->
                    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
                    <?php if ($this->session->flashdata('flash')) : ?>
                    <?php endif; ?>
                    <div class="card">
                        <div class="card-header">
                            Detail Transaksi
                        </div>
                        <div id="backback" class="card-body">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="form-group">
                                            <input type="hidden" name="subcategory_id" value="<?= $agenda['subcategory_id']; ?>">

                                            <span for="id">ID Pelanggan : </span><span style="font-weight: bold;"><?= $agenda['kodeToko']; ?></span><br>
                                            <span for="id">Tanggal : </span><span><?= $agenda['tanggal_transaksi']; ?></span><br>
                                            <!-- <input type="text" onkeyup="kapitalKata()" name="nama" class="form-control" id="nama" value="<?= $agenda['kodeToko']; ?>" readonly>
                                            <small class="form-text text-danger"><?= form_error('nama'); ?></small> -->

                                            <span for="namaPelanggan">Nama Pelanggan : <?= $agenda['subcategory_name']; ?></span><br>
                                            <!-- <input type="text" onkeyup="kapitalKata()" name="namaPelanggan" class="form-control" id="namaPelanggan" value="<?= $agenda['subcategory_name']; ?>" required> -->
                                            <!-- <small class="form-text text-danger"><?= form_error('namaPelanggan'); ?></small> -->

                                            <span for="kota">Kota : <?= $agenda['subcategory_category_id']; ?></span><br>
                                            <!-- <input type="text" onkeyup="kapitalKata()" name="kota" class="form-control" id="kota" value="<?= $agenda['subcategory_category_id']; ?>" readonly> -->
                                            <!-- <small class="form-text text-danger"><?= form_error('kota'); ?></small> -->

                                            <span for="alamat">Alamat : <?= $agenda['alamat_toko']; ?></span><br>
                                            <!-- <input type="text" onkeyup="kapitalKata()" name="alamat" class="form-control" id="alamat" value="<?= $agenda['alamat_toko']; ?>" required> -->
                                            <!-- <small class="form-text text-danger"><?= form_error('alamat'); ?></small> -->

                                            <span for="telp">No. Telp : <?= $agenda['telp']; ?></span><br>
                                            <!-- <input type="number" name="telp" class="form-control mb-3" id="telp" value="<?= $agenda['telp']; ?>" required> -->
                                            <!-- <small class="form-text text-danger"><?= form_error('telp'); ?></small> -->

                                            <span for="numberWA">No. WA : <?= $agenda['nowa']; ?></span>
                                            <!-- <input type="number" name="numberWA" class="form-control mb-3" id="numberWA" value="<?= $agenda['nowa']; ?>"> -->
                                            <!-- <small class="form-text text-danger"><?= form_error('numberWA'); ?></small> -->
                                            <input type="number" min="0" class="form-control mt-2" name="cicilan" id="kolomHutang">
                                            <input type="number" value="<?= $agenda['dibayar'] ?>" class="form-control mt-2" name="cicilanLama" hidden>
                                            <small class="form-text text-danger" id="errorHutang"></small>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="form-group">
                                            <div class="table-responsive">

                                                <table id="tabelBarang" class="table table-striped" width="100%" cellspacing="0">
                                                    <tr>
                                                        <center>
                                                            <label style="font-weight: bold;">
                                                                DAFTAR BARANG
                                                            </label>
                                                        </center>
                                                    </tr>
                                                    <tr style="text-align: center;">
                                                        <th>
                                                            No
                                                        </th>
                                                        <th>
                                                            Nama Produk
                                                        </th>
                                                        <th>
                                                            Satuan
                                                        </th>
                                                        <th>
                                                            Harga
                                                        </th>
                                                        <th>
                                                            Jumlah
                                                        </th>
                                                        <th>
                                                            Total
                                                        </th>
                                                        <!-- <th>
                                                            Aksi
                                                        </th> -->
                                                    </tr>
                                                    <?php
                                                    $nooo = 1;
                                                    foreach ($join as $buku) {
                                                    ?>
                                                        <tr>
                                                            <td style="text-align: center;">
                                                                <?= $nooo++; ?>
                                                            </td>
                                                            <td hidden>
                                                                <?= $buku->id; ?>
                                                            </td>
                                                            <td nowrap>
                                                                <?= $buku['id_bjadi']; ?>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <?= $buku['satuan']; ?>
                                                            </td>
                                                            <td style="text-align: right;" nowrap>
                                                                <?= "Rp. " . number_format($buku['harga'], 0, ".", "."); ?>
                                                            </td>
                                                            <td style="text-align: right;" nowrap>
                                                                <?= $buku['jumlah']; ?>
                                                            </td>
                                                            <td style="text-align: right;" nowrap>
                                                                <?= "Rp. " . number_format($buku['total'], 0, ".", "."); ?>
                                                            </td>
                                                            <!-- <td nowrap style="text-align: center;">
                                                                <a class='remove' type='button'><i class='fas fa-fw fa-minus-circle'></i></a><a class='edit' type='button'><i class='fas fa-fw fa-edit'></i></a>
                                                            </td> -->
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>

                                                    <!-- <tr id="tabelBarang2">
                                                    </tr> -->
                                                </table>

                                                <!-- <table class="table table-striped" width="100%" cellspacing="0">
                                                    <tr class="inputProduk">
                                                        <th>
                                                            <div class="input-group-sm">
                                                                <select name="pilihProduk" id="pilihProduk" class="form-control" style="width:auto">
                                                                    <option value="">Produk</option>
                                                                    <?php foreach ($produk as $nama1) : ?>

                                                                        <option value="<?= $nama1['namaBarang']; ?>" <?= (set_value('pilihProduk') == $nama1['namaBarang'] ? ' selected' : ''); ?>>
                                                                            <?= $nama1['namaBarang']; ?>
                                                                        </option>

                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div class="input-group-sm">
                                                                <select class="form-control" id="satuan" style="width:auto">
                                                                    <option>PCS</option>
                                                                </select>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div class="input-group-sm harga">
                                                                <input type="number" class="form-control" id="harga" min="0">
                                                            </div>
                                                        </th>
                                                        <th style="text-align: center;">
                                                            <a class="add-more" type="button"><i class="fas fa-plus-circle"></i></a>
                                                        </th>
                                                    </tr>
                                                </table> -->
                                            </div>
                                            <div class="row-md">
                                                <div class="col mb-2" style="text-align: right;">

                                                    <span style="font-weight: bold;" id="subtotal"></span><br>
                                                    <span style="font-weight: bold;" id="bayar">Dibayar = <?= "Rp. " . number_format($agenda['dibayar'], 0, ".", "."); ?></span><br>
                                                    <span style="font-weight: bold;" id="sisa"></span>
                                                </div>
                                                <!-- <table>
                                                    <tr>
                                                        <th>
                                                            TOTAL =
                                                        </th>
                                                        <th id="subtotal">

                                                        </th>
                                                    </tr>
                                                </table> -->
                                            </div>
                                            <div class="col">
                                                <a href="<?= base_url();  ?>pelanggan/lunas/<?= $agenda['subcategory_id']; ?>" id="lunas"><input class="btn btn-success float-right" type="button" value="Lunas"></a>
                                                <button type="submit" id="add_data" name="ubah" class="btn btn-primary float-right" disabled='disabled'>Ubah Data</button>
                                                <a href="<?= base_url('pelanggan'); ?>"><input class="mr-2 btn btn-secondary float-right" type="button" value="Kembali"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </main>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".add-more").click(function() {
                var id_pelanggan = $('#nama').val();
                var id_bjadi = $('#pilihProduk').val();
                var satuan = $('#satuan').val();
                var harga = $('#harga').val();
                $.ajax({
                    type: "POST",
                    async: true,
                    data: {
                        id_pelanggan: id_pelanggan,
                        id_bjadi: id_bjadi,
                        satuan: satuan,
                        harga: harga,
                        // tanggal: tanggal,

                    },
                    dataType: "text",
                    url: "<?= base_url('pelanggan/tambahBarang'); ?>",
                    success: function(json) {
                        var data = "";
                        if (json != "error") {
                            data = "<td></td><td id='" + json + "'>" + id_bjadi + "</td><td id='" + json + "' style='text-align: center;'>" + satuan + "</td><td id='" + json + "'>" + harga + "</td><td id='" + json + "' style='text-align: center;'><a class='remove' type='button'><i class='fas fa-fw fa-minus-circle'></i></a><a class='edit' type='button'><i class='fas fa-fw fa-edit'></i></a></td>";
                            $('#tabelBarang2').append(data);
                            $('#harga').val('');
                            $('#pilihProduk').val('');
                            location.reload();
                        }
                    }
                })
            });

            var tabel = document.querySelector('#tabelBarang');
            var sumHsl = 0;
            for (var t = 2; t < tabel.rows.length; t++) {
                // console.log(tabel.rows[t].cells[6].innerText);
                let dataH = tabel.rows[t].cells[6].innerText;
                editedit = dataH.replace('Rp. ', '');
                editfix = editedit.replace(/\./g, '');
                sumHsl = sumHsl + parseInt(editfix);
                // membuat format rupiah field Sub Total
                var number_string4 = sumHsl.toString(),
                    sisa4 = number_string4.length % 3,
                    rupiah4 = 'Rp. ' + number_string4.substr(0, sisa4),
                    ribuan4 = number_string4.substr(sisa4).match(/\d{3}/g);

                if (ribuan4) {
                    separator4 = sisa4 ? '.' : '';
                    rupiah4 += separator4 + ribuan4.join('.');
                }
            }
            document.getElementById("subtotal").innerHTML = 'TOTAL = ' + rupiah4;

            let zxc = document.getElementById("subtotal").innerHTML;
            qwer = zxc.replace('TOTAL = Rp. ', '');
            frew = qwer.replace(/\./g, '');
            let rtuyrty = parseInt(frew) - <?= $agenda['dibayar'] ?>;
            var nsa = rtuyrty.toString(),
                ssqqww = nsa.length % 3,
                rpa = 'Rp. ' + nsa.substr(0, ssqqww),
                rtsa = nsa.substr(ssqqww).match(/\d{3}/g);

            if (rtsa) {
                separator4 = ssqqww ? '.' : '';
                rpa += separator4 + rtsa.join('.');
            }


            document.getElementById('sisa').innerHTML = 'Sisa = ' + rpa;
            if (rpa == 'Rp. 0') {
                document.getElementById("kolomHutang").hidden = true;
                document.getElementById("add_data").hidden = true;
                // document.getElementById("backback").style.background = 'url(<?= base_url('assets/lunas.png') ?>)';
                // document.getElementById("backback").style.backgroundRepeat = 'no-repeat';
                // document.getElementById("backback").style.backgroundSize = '300px 150px';
                // document.getElementById("backback").style.backgroundPosition = 'left bottom';
            } else {
                document.getElementById("lunas").hidden = true;
            }

            tabel.addEventListener('click', function(e) {
                if (e.target.parentElement.parentElement.className == 'remove') {
                    if (confirm('Yakin dihapus?')) {
                        var id = e.target.parentElement.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.innerText;
                        $.ajax({
                            type: "POST",
                            async: true,
                            data: {
                                id: id,
                            },
                            dataType: "text",
                            url: "<?= base_url('pelanggan/hapusBarang'); ?>",
                            success: function(json) {
                                if (json != "error") {
                                    // $('[id=' + id + ']').remove();
                                    // $('#tabelBarang').append();
                                }
                            }
                        })
                        e.target.parentElement.parentElement.parentElement.parentElement.style.display = 'none';
                        location.reload();
                        e.preventDefault();
                    }
                } else if (e.target.parentElement.parentElement.className == 'edit') {
                    if (confirm('Yakin diubah?')) {
                        var id = e.target.parentElement.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.innerText;
                        $.ajax({
                            type: "POST",
                            async: true,
                            data: {
                                id: id,
                            },
                            dataType: "text",
                            url: "<?= base_url('pelanggan/hapusBarang'); ?>",
                            success: function(json) {
                                if (json != "error") {
                                    // $('[id=' + id + ']').remove();
                                    // $('#tabelBarang').append();
                                }
                            }
                        })
                        $('#pilihProduk').val(e.target.parentElement.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.innerText);
                        const str = e.target.parentElement.parentElement.parentElement.previousElementSibling.innerText;
                        const res = str.slice(4);
                        const outStr = res.replace(/[^\w\s]/gi, '')
                        $('#harga').val(outStr);
                        e.target.parentElement.parentElement.parentElement.parentElement.style.display = 'none';
                        // location.reload();
                        e.preventDefault();
                    }
                }
            });
        });

        $(document).change(function() {
            // set max inputan hutang
            let maX = document.getElementById("sisa").innerHTML;
            ma2e = maX.replace('Sisa = ', '');
            ma2 = maX.replace('Sisa = Rp. ', '');
            jadi = ma2.replace(/\./g, '');
            document.getElementById("kolomHutang").max = jadi;
            if (parseInt(document.getElementById("kolomHutang").value) > parseInt(jadi)) {
                document.getElementById('errorHutang').innerHTML = 'Inputan tidak boleh lebih dari ' + ma2e;
                document.getElementById("add_data").setAttribute("disabled", "disabled");
                return;
            } else {
                document.getElementById("errorHutang").innerHTML = "";
                document.getElementById("add_data").removeAttribute("disabled");
                return;
            }
        });
    </script>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- </div> -->