<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- Breadcrumbs-->
                    <ol class="breadcrumb mt-2">
                        <?php foreach ($this->uri->segments as $segment) : ?>
                            <?php
                            $url = substr($this->uri->uri_string, 0, strpos($this->uri->uri_string, $segment)) . $segment;
                            $is_active =  $url == $this->uri->uri_string;
                            ?>
                            <li class="breadcrumb-item <?php echo $is_active ? 'active' : '' ?>">
                                <?php if ($is_active) : ?>
                                    <!-- <?php echo ucfirst($segment) ?> -->
                                    <?= ucfirst("bahan Baku") ?>
                                <?php else : ?>
                                    <a href="<?php echo site_url($url) ?>"><?php echo ucfirst($segment) ?></a>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ol>
                    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
                    <?php if ($this->session->flashdata('flash')) : ?>
                    <?php endif; ?>
                    <div class="col mb-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>PENGATURAN BAHAN BAKU</b></div>
                    <a href="<?= base_url('pengaturan/addBaku'); ?>" class="btn btn-primary mb-2">Tambah Data</a>
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                                    <thead style="font-size: small; text-align: center;">
                                        <tr>
                                            <th>No</th>
                                            <th>Kategori</th>
                                            <th>Kode</th>
                                            <th>Produk</th>
                                            <th>Satuan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: small;">
                                        <?php
                                        $no = 1;
                                        foreach ($join as $buku) {
                                        ?>
                                            <tr style="text-align: center;">
                                                <td><?= $no++; ?></td>
                                                <td><?= $buku->kategori; ?></td>
                                                <td><?= $buku->kode; ?></td>
                                                <td><?= $buku->produk; ?></td>
                                                <td><?= $buku->satuan; ?></td>
                                                <td align="center">
                                                    <a href="<?= base_url();  ?>pengaturan/ubahBaku/<?= $buku->idBaku; ?>"><i class="fas fa-fw fa-edit"></i></a>
                                                    <a href="<?= base_url();  ?>pengaturan/hapusBaku/<?= $buku->idBaku; ?>" class="tombol-hapus"><i class="fas fa-fw fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End of Main Content -->
                </div>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>