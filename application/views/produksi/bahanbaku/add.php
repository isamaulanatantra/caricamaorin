<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="col my-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>TAMBAH DATA BAHAN BAKU</b></div>
            <div class="col-4 mt-2">
                <form method="POST">
                    <div class="form-group">
                        <label>Kategori</label>
                        <select class="form-control mb-2" id="kategori" name="kategori" autofocus>
                            <option value=""></option>
                            <option value="Masak" <?php if (set_value('kategori') == 'Masak') {
                                                        echo "selected";
                                                    } ?>>Masak</option>
                            <option value="Packing" <?php if (set_value('kategori') == 'Packing') {
                                                        echo "selected";
                                                    } ?>>Packing</option>
                        </select>
                        <small class="form-text text-danger"><?= form_error('kategori'); ?></small>
                    </div>
                    <div class="form-group">
                        <label>Kode</label>
                        <input type="text" id="kode" name="kode" class="form-control" value="<?= set_value('kode'); ?>" onkeyup="kapitalAll()" required>
                        <small class="form-text text-danger"><?= form_error('kode'); ?></small>
                    </div>
                    <div class="form-group">
                        <label>Produk</label>
                        <input type="text" id="produk" name="produk" class="form-control" onkeyup="kapitalKata()" value="<?= set_value('produk'); ?>" required>
                        <small class="form-text text-danger"><?= form_error('produk'); ?></small>
                    </div>
                    <div class="form-group">
                        <label>Satuan</label>
                        <select class="form-control mb-2" id="satuan" name="satuan" required>
                            <option value=""></option>
                            <option value="KG" <?php if (set_value('satuan') == 'KG') {
                                                    echo "selected";
                                                } ?>>KG</option>
                            <option value="Pcs" <?php if (set_value('satuan') == 'Pcs') {
                                                    echo "selected";
                                                } ?>>Pcs</option>
                            <option value="Zak" <?php if (set_value('satuan') == 'Zak') {
                                                    echo "selected";
                                                } ?>>Zak</option>
                        </select>
                    </div>
                    <div style="text-align: center;">
                        <a type="submit" href="<?= base_url('pengaturan/bbaku') ?>" class="btn btn-secondary">Kembali</a>
                        <button type="submit" id="add_data" class="btn btn-primary" disabled="disabled">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous">
        </script>
        <script>
            function kapitalAll() {
                var x = document.getElementById("kode");
                x.value = x.value.toUpperCase();
            }

            function kapitalKata() {
                var t = document.getElementById("produk");
                t.value = Capitalize(t.value);
            }

            function Capitalize(str) {
                return str.replace(/\w\S*/g,
                    function(txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
            }

            (function() {
                $(document.getElementsByTagName('input')).keyup(function() {
                    var fff = document.getElementById('kategori');
                    // console.log(fff.value);
                    var empty = false;
                    $(document.getElementsByTagName('input') && document.getElementById('kategori')).each(function() {
                        if ($(this).val() == '') {
                            empty = true;
                        }
                    });

                    if (empty) {
                        $('#add_data').attr('disabled', 'disabled');
                    } else {
                        $('#add_data').removeAttr('disabled');
                    }
                });
            })()
        </script>

    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>