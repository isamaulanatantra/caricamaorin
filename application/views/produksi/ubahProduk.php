<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row mt-3">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <!-- <h1 class="h3 mb-4 text-gray-800"><?= $judul; ?></h1> -->
                            Form Ubah Data Kota
                        </div>
                        <div class="card-body">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input type="hidden" name="id" value="<?= $agenda['idBarang']; ?>">
                                            <div class="form-group">
                                                <label>Kategori</label>
                                                <input type="text" id="kategori" name="kategori" class="form-control" value="<?= $agenda['kategori']; ?>" autofocus required>
                                                <small class="form-text text-danger"><?= form_error('kategori'); ?></small>
                                            </div>
                                            <div class="form-group">
                                                <label>Kode</label>
                                                <input type="text" id="kode" name="kode" class="form-control" value="<?= $agenda['kode']; ?>" onkeyup="kapitalAll()" required>
                                                <small class="form-text text-danger"><?= form_error('kode'); ?></small>
                                            </div>
                                            <div class="form-group">
                                                <label>Produk</label>
                                                <input type="text" id="produk" name="produk" class="form-control" value="<?= $agenda['namaBarang']; ?>" required>
                                                <small class="form-text text-danger"><?= form_error('produk'); ?></small>
                                            </div>
                                            <div class="form-group">
                                                <label>Satuan</label>
                                                <select class="form-control" id="satuan" name="satuan">
                                                    <option>PCS</option>
                                                </select>
                                                <small class="form-text text-danger"><?= form_error('satuan'); ?></small>
                                            </div>
                                            <div style="text-align: center;">
                                                <a type="submit" href="<?= base_url('produksi/produk') ?>" class="btn btn-secondary">Kembali</a>
                                                <button type="submit" name="ubah" class="btn btn-primary">Ubah Data</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- </div> -->