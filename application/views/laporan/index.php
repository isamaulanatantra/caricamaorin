<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- <?= var_dump($umasuk) ?> -->
                    <?php if (validation_errors()) : ?>
                        <div class="alert alert-danger" role="alert">
                            <?= validation_errors(); ?>
                        </div>
                    <?php endif; ?>

                    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
                    <?php if ($this->session->flashdata('flash')) : ?>
                    <?php endif; ?>
                    <div class="col my-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>LAPORAN</b></div>
                    <span id="totalUang" style="font-weight: bold;">Total uang diluar = <?= "Rp. " . number_format($uangkeluar['TOTALUANG'], 0, ".", "."); ?></span><br>
                    <span id="uMasuk" style="font-weight: bold;">Total uang masuk = <?= "Rp. " . number_format($umasuk['tdibayar'], 0, ".", "."); ?></span><br>
                    <span id="totalProduk" style="font-weight: bold;">Total Produk Terjual = <?= $pKeluar['jumlahproduk']; ?></span>

                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                // let nuumm = <?= $uangkeluar['TOTALUANG'] ?>;
                // var number_string4 = nuumm.toString(),
                //     sisa4 = number_string4.length % 3,
                //     rupiah4 = 'Rp. ' + number_string4.substr(0, sisa4),
                //     ribuan4 = number_string4.substr(sisa4).match(/\d{3}/g);

                // if (ribuan4) {
                //     separator4 = sisa4 ? '.' : '';
                //     rupiah4 += separator4 + ribuan4.join('.');
                // }
                // document.getElementById("totalUang").innerHTML = 'Total Uang Diluar Berupa Produk = ' + rupiah4;
            });
        </script>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- </div> -->