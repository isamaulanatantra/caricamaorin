<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <?php if (validation_errors()) : ?>
                        <div class="alert alert-danger" role="alert">
                            <?= validation_errors(); ?>
                        </div>
                    <?php endif; ?>

                    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
                    <?php if ($this->session->flashdata('flash')) : ?>
                    <?php endif; ?>
                    <div class="col my-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>DATA KARYAWAN</b></div>
                    <a href="<?= base_url('karyawan/tambahKaryawan'); ?>" class="btn btn-primary mb-2">Tambah Data</a>
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                                    <thead style="text-align:center">
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Jabatan</th>
                                            <th>Alamat</th>
                                            <th>Kontak</th>
                                            <th style="width:125px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody style="text-align: center;">
                                        <?php
                                        $no = 1;
                                        foreach ($pegawai as $buku) {
                                        ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td><?= $buku->nama; ?></td>
                                                <td><?= $buku->jabatan; ?></td>
                                                <td><?= $buku->alamat; ?></td>
                                                <td><?= $buku->kontak; ?></td>
                                                <td style="text-align: center;">
                                                    <a href="<?= base_url();  ?>karyawan/ubahKaryawan/<?= $buku->id_pegawai; ?>"><i class="fas fa-fw fa-edit"></i></a>
                                                    <a href="<?= base_url();  ?>karyawan/hapusKaryawan/<?= $buku->id_pegawai; ?>" class="tombol-hapus"><i class="fas fa-fw fa-trash-alt"></i></i></a>

                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End of Main Content -->


                </div>
            </div>
        </div>
        <script>
            function kapitalKata() {
                var t = document.getElementById("nama");
                t.value = Capitalize(t.value);
            }

            function Capitalize(str) {
                return str.replace(/\w\S*/g,
                    function(txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
            }
        </script>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- </div> -->