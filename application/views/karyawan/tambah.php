<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="col my-2" style="text-align: center; color: white; background-color: #5DADE2;"><b>TAMBAH DATA KARYAWAN</b></div>
            <div class="col-4 mt-2">
                <form method="POST">
                    <div class="form-group">
                        <input type="text" class="form-control mb-2" onkeyup="kapitalKata()" id="nama" name="nama" placeholder="Masukkan Nama..." autofocus required>
                        <select class="form-control mb-2" id="jabatan" name="jabatan" required>
                            <option value="">Pilih Jabatan</option>
                            <option value="Owner">Owner</option>
                            <option value="Akuntansi">Akuntansi</option>
                            <option value="Driver">Driver</option>
                            <option value="Sales">Sales</option>
                            <option value="Packing">Packing</option>
                            <option value="Produksi">Produksi</option>
                        </select>
                        <input type="text" class="form-control mb-2" id="alamat" onkeyup="kapitalKata()" name="alamat" placeholder="Masukkan Alamat..." required>
                        <input type="number" class="form-control" id="kontak" name="kontak" min="0" placeholder="Masukkan Kontak..." required>
                    </div>
                    <div style="text-align: center;">
                        <a type="submit" href="<?= base_url('karyawan') ?>" class="btn btn-secondary">Kembali</a>
                        <button id="add_data" type="submit" class="btn btn-primary" disabled="disabled">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>
            $(function() {
                $("#jabatan").on('change', function() {
                    if ($(this).val() == "")
                        $("#add_data").attr('disabled', true)
                    else
                        // $("#add_data").attr('disabled', true)
                        (function() {
                            $(document.getElementsByTagName('input')).keyup(function() {
                                var empty = false;
                                $(document.getElementsByTagName('input')).each(function() {
                                    if ($(this).val() == '') {
                                        empty = true;
                                    }
                                });

                                if (empty) {
                                    $('#add_data').attr('disabled', 'disabled');
                                } else {
                                    $('#add_data').removeAttr('disabled');
                                }
                            });
                        })()
                });
            });

            function kapitalKata() {
                var t = document.getElementById("nama");
                var q = document.getElementById("alamat");
                t.value = Capitalize(t.value);
                q.value = Capitalize(q.value);
            }

            function Capitalize(str) {
                return str.replace(/\w\S*/g,
                    function(txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
            }
            // (function() {
            //     $(document.getElementsByTagName('input')).keyup(function() {
            //         var empty = false;
            //         $(document.getElementsByTagName('input')).each(function() {
            //             if ($(this).val() == '') {
            //                 // document.getElementById('jabatan').value == '';
            //                 empty = true;
            //             }
            //         });

            //         if (empty) {
            //             $('#add_data').attr('disabled', 'disabled');
            //         } else {
            //             $('#add_data').removeAttr('disabled');
            //         }
            //     });
            // })()
        </script>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>