<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row mt-3">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <!-- <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1> -->
                            Form Ubah Data
                        </div>
                        <div class="card-body">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input type="hidden" name="id_pegawai" value="<?= $agenda['id_pegawai']; ?>">
                                            <label for="nama">Nama</label>
                                            <input type="text" onkeyup="kapitalKata()" name="nama" class="form-control" id="nama" value="<?= $agenda['nama']; ?>" required>
                                            <small class="form-text text-danger"><?= form_error('nama'); ?></small>

                                            <label for="alamat">Jabatan</label>
                                            <select class="form-control" id="jabatan" name="jabatan" required>
                                                <option value="">Pilih Jabatan</option>
                                                <option value="Owner" <?php if ($agenda['jabatan'] == 'Owner') {
                                                                            echo "selected";
                                                                        } ?>>Owner</option>
                                                <option value="Akuntansi" <?php if ($agenda['jabatan'] == 'Akuntansi') {
                                                                                echo "selected";
                                                                            } ?>>Akuntansi</option>
                                                <option value="Driver" <?php if ($agenda['jabatan'] == 'Driver') {
                                                                            echo "selected";
                                                                        } ?>>Driver</option>
                                                <option value="Sales" <?php if ($agenda['jabatan'] == 'Sales') {
                                                                            echo "selected";
                                                                        } ?>>Sales</option>
                                                <option value="Packing" <?php if ($agenda['jabatan'] == 'Packing') {
                                                                            echo "selected";
                                                                        } ?>>Packing</option>
                                                <option value="Produksi" <?php if ($agenda['jabatan'] == 'Produksi') {
                                                                                echo "selected";
                                                                            } ?>>Produksi</option>
                                            </select>
                                            <label for="alamat">Alamat</label>
                                            <input type="text" onkeyup="kapitalKata()" name="alamat" class="form-control" id="alamat" value="<?= $agenda['alamat']; ?>" required>
                                            <small class="form-text text-danger"><?= form_error('alamat'); ?></small>
                                            <label for="kontak">Kontak</label>
                                            <input type="number" name="kontak" class="form-control mb-3" id="kontak" value="<?= $agenda['kontak']; ?>" required>
                                            <small class="form-text text-danger"><?= form_error('kontak'); ?></small>
                                            <button type="submit" name="ubah" class="btn btn-primary float-right">Ubah Data</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        function kapitalKata() {
            var t = document.getElementById("nama");
            var q = document.getElementById("alamat");
            t.value = Capitalize(t.value);
            q.value = Capitalize(q.value);
        }

        function Capitalize(str) {
            return str.replace(/\w\S*/g,
                function(txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
        }
    </script>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Carica Maorin 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- </div> -->