<?php

class Produksi_model extends CI_Model
{
    // Produksi Produk
    function getProduk()
    {
        $this->db->select('*');
        $this->db->from('bjadi');
        $this->db->where('status = 1');
        $this->db->order_by('namaBarang', 'ASC');
        $query = $this->db->get();
        return $query;
    }

    public function tambahDataProduk()
    {
        $data = [
            "kategori" => $this->input->post('kategori', true),
            "kode" => $this->input->post('kode', true),
            "satuan" => $this->input->post('satuan', true),
            "min_harga" => $this->input->post('min', true),
            "max_harga" => $this->input->post('max', true),
            "status" => 1,
            "namaBarang" => $this->input->post('produk', true)
        ];

        $this->db->insert('bjadi', $data);
    }

    public function hapusDataProv($id)
    {
        $data = [
            "status" => '0'
        ];

        $this->db->where('idBarang', $id);
        $this->db->update('bjadi', $data);
    }

    public function getAgendaById($id)
    {
        return $this->db->get_where('bjadi', ['idBarang' => $id])->row_array();
    }

    public function ubahDataKota()
    {
        $data = [
            "kategori" => $this->input->post('kategori', true),
            "kode" => $this->input->post('kode', true),
            "satuan" => $this->input->post('satuan', true),
            "namaBarang" => $this->input->post('produk', true)
        ];

        $this->db->where('idBarang', $this->input->post('idBarang'));
        $this->db->update('bjadi', $data);
    }

    // Produksi Bahan Baku
    function getBaku()
    {
        $this->db->select('*');
        $this->db->from('bbaku');
        $this->db->where('status = 1');
        $this->db->order_by('produk', 'ASC');
        $query = $this->db->get();
        return $query;
    }

    public function tambahDataBaku()
    {
        $data = [
            "kategori" => $this->input->post('kategori', true),
            "kode" => $this->input->post('kode', true),
            "satuan" => $this->input->post('satuan', true),
            "status" => 1,
            "produk" => $this->input->post('produk', true)
        ];

        $this->db->insert('bbaku', $data);
    }

    public function hapusDataBaku($id)
    {
        $data = [
            "status" => '0'
        ];

        $this->db->where('idBaku', $id);
        $this->db->update('bbaku', $data);
    }

    public function getBakuById($id)
    {
        return $this->db->get_where('bbaku', ['idBaku' => $id])->row_array();
    }

    public function ubahDataBaku()
    {
        $data = [
            "kategori" => $this->input->post('kategori', true),
            "kode" => $this->input->post('kode', true),
            "satuan" => $this->input->post('satuan', true),
            "produk" => $this->input->post('produk', true)
        ];

        $this->db->where('idBaku', $this->input->post('idBaku'));
        $this->db->update('bbaku', $data);
    }
}
