<?php

class Pelanggan_model extends CI_Model
{
    // Fungsi untuk melakukan proses upload file
    public function upload()
    {
        $config['upload_path'] = './assets/upload/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        // $config['max_size']    = '2048';
        $config['remove_space'] = TRUE;
        // if ($config['file_name'] == null) {
        //     $config['file_name'] = 'default.jpg';
        // }
        // print_r($config);
        // die();

        $this->load->library('upload', $config); // Load konfigurasi uploadnya
        if ($this->upload->do_upload('inputFile')) { // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        } else {
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            // $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    function search_blog($title)
    {
        $this->db->like('nama_kota', $title, 'both');
        $this->db->order_by('nama_kota', 'ASC');
        $this->db->limit(10);
        return $this->db->get('kota')->result();
    }

    // Fungsi untuk menyimpan data ke database
    public function save($upload)
    {
        $data = array(
            'email' => $this->input->post('email'),
            'telp' => $this->input->post('telp'),
            'nowa' => $this->input->post('waa'),
            'kodeToko' => $this->input->post('idPelanggan'),
            'subcategory_name' => $this->input->post('nama'),
            'alamat_toko' => $this->input->post('alamat'),
            'tanggal_transaksi' => $this->input->post('tanggal'),
            'subcategory_category_id' => $this->input->post('nKota'),
            'kategori' => $this->input->post('kategori'),
            'latitude' => $this->input->post('lat'),
            'longitude' => $this->input->post('long'),
            'status' => 1,
            'namaPersonal' => $this->input->post('namaPersonal'),
            // 'foto' => $upload['file']['file_name']
            'foto' => $upload
            // 'ukuran_file' => $upload['file']['file_size'],
            // 'tipe_file' => $upload['file']['file_type']
        );

        $this->db->insert('pelanggan', $data);
    }

    function get_products()
    {
        $this->db->select('subcategory_id,subcategory_category_id,latitude,longitude,foto,telp,email,nowa,subcategory_name,alamat_toko,kodeToko,subcategory_category_id');
        $this->db->from('pelanggan');
        $this->db->where('lunas = 0');
        // $this->db->join('kota', 'kode_prov = kode_prov', 'left');
        $this->db->order_by('kodeToko', 'ASC');
        $query = $this->db->get();
        return $query;
    }

    function get_pembelian($id)
    {
        $this->db->select('*');
        $this->db->where(['subcategory_id' => $id]);
        $this->db->from('pelanggan');
        $this->db->join('pembelian', 'id_pelanggan = kodeToko', 'left');
        $query = $this->db->get()->result_array();
        return $query;
    }

    function getNamaKota($id)
    {
        $this->db->select('*');
        $this->db->where(['subcategory_id' => $id]);
        $this->db->from('toko');
        $this->db->join('kota', 'subcategory_category_id = kode_kota', 'left');
        // $this->db->order_by('kodeToko', 'ASC');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getAgendaById($id)
    {
        return $this->db->get_where('toko', ['subcategory_id' => $id])->row_array();
    }

    function get_category()
    {
        $query = $this->db->get('kota');
        $query = $this->db->order_by('category_name', 'ASC');
        return $query;
    }

    function get_product_by_id($product_id)
    {
        $query = $this->db->get_where('toko', array('subcategory_id' =>  $product_id));
        return $query;
    }

    public function noUrut()
    {
        $this->db->select('COUNT(subcategory_id) AS noUrut');
        $this->db->from('pelanggan');
        // $this->db->order_by('nama_kota', 'ASC');
        $query = $this->db->get()->row_array();
        return $query;
    }

    public function tambahDataMahasiswa($upload)
    {
        $data = [
            // "subcategory_category_id" => $this->input->post('nKota', true),
            // "regency_id" => $this->input->post('nKota', true),
            // "kodeToko" => $this->input->post('id', true),
            // "foto" => $this->input->post('berkas', true),
            'foto' => $upload['file']['file_name'],
            // "telp" => $this->input->post('telp', true),
            // "email" => $this->input->post('email', true),
            // "kategori" => $this->input->post('kategori', true),
            // "subcategory_name" => $this->input->post('nama', true),
            // "alamat_toko" => $this->input->post('alamat', true),
            // "hargaBaruP6" => 123,
            // "hargaBaruD6" => 123,
            // "hargaBaruD12" => 123,
            // "hargaBaruCupB" => 123,
            // "hargaBaruCupM" => 123,
            // "hargaBaruBotol" => 123
            // "hargaBaruP6" => $this->input->post('harga1', true),
            // "hargaBaruD6" => $this->input->post('harga2', true),
            // "hargaBaruD12" => $this->input->post('harga3', true),
            // "hargaBaruCupB" => $this->input->post('harga4', true),
            // "hargaBaruCupM" => $this->input->post('harga5', true),
            // "hargaBaruBotol" => $this->input->post('harga6', true)

        ];

        $this->db->insert('toko', $data);
    }

    public function ubahPelanggan($id)
    {
        $data = [
            "status" => 0,
        ];
        $this->db->set('tanggalHapus', 'NOW()', FALSE);
        $this->db->where('subcategory_id', $id);
        $this->db->update('pelanggan', $data);
    }

    public function lunasPelanggan($id)
    {
        $data = [
            "lunas" => 1,
        ];
        $this->db->where('subcategory_id', $id);
        $this->db->update('pelanggan', $data);
    }

    public function ubahDataPelanggan()
    {
        $data = [
            // "subcategory_name" => $this->input->post('nama', true),
            "dibayar" => $this->input->post('cicilan', true) + $this->input->post('cicilanLama', true),
            // "dibayar" => $this->input->post('cicilan', true) + $this->input->post('cicilanLama', true),
            // "subcategory_name" => $this->input->post('namaPelanggan', true),
            // "subcategory_category_id" => $this->input->post('kota', true),
            // "alamat_toko" => $this->input->post('alamat', true),
            // "telp" => $this->input->post('telp', true),
            // "nowa" => $this->input->post('numberWA', true)
        ];

        $this->db->where('subcategory_id', $this->input->post('subcategory_id'));
        $this->db->update('pelanggan', $data);
    }

    public function ubahDataTokoUser()
    {
        $data = [
            "subcategory_category_id" => $this->input->post('category', true),
            "regency_id" => $this->input->post('category', true),
            "kodeToko" => $this->input->post('kToko', true),
            "subcategory_name" => $this->input->post('nToko', true),
            "alamat_toko" => $this->input->post('alamat', true),
        ];

        $this->db->where('subcategory_id', $this->input->post('subcategory_id'));
        $this->db->update('toko', $data);
    }

    public function ambilProduk()
    {
        $this->db->select('*');
        $this->db->from('bjadi');
        $this->db->where('status = 1');
        $this->db->order_by('idBarang', 'ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function tambahTokoUser()
    {
        $data = [
            "subcategory_category_id" => $this->input->post('nKota', true),
            "regency_id" => $this->input->post('nKota', true),
            "kodeToko" => $this->input->post('kToko', true),
            "subcategory_name" => $this->input->post('nToko', true),
            "alamat_toko" => $this->input->post('alamat', true)
        ];

        $this->db->insert('toko', $data);
    }

    public function getPelangganById($id)
    {
        // return $this->db->get_where('pelanggan', ['subcategory_id' => $id])->row_array();
        $this->db->select('*');
        $this->db->where(['subcategory_id' => $id]);
        $this->db->from('pelanggan');
        $this->db->join('pembelian', 'id_pelanggan = kodeToko', 'left');
        // $this->db->order_by('kodeToko', 'ASC');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function hapusDataPelanggan($id)
    {
        $this->db->delete('pelanggan', ['subcategory_id' => $id]);
    }

    function hapus_barang($kode)
    {
        // $hasil = $this->db->query("DELETE FROM pembelian WHERE id='$kode'");
        $hasil = $this->db->delete('pembelian', ['id' => $kode]);
        return $hasil;
    }
}
