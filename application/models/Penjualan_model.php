<?php

class Penjualan_model extends CI_Model
{
    function get_products()
    {
        $this->db->select('subcategory_id,subcategory_category_id,latitude,longitude,foto,telp,email,nowa,subcategory_name,alamat_toko,kodeToko,subcategory_category_id');
        $this->db->from('pelanggan');
        $this->db->where('lunas = 1');
        // $this->db->join('kota', 'kode_prov = kode_prov', 'left');
        $this->db->order_by('kodeToko', 'ASC');
        $query = $this->db->get();
        return $query;
    }

    public function getPelangganById($id)
    {
        // return $this->db->get_where('pelanggan', ['subcategory_id' => $id])->row_array();
        $this->db->select('*');
        $this->db->where(['subcategory_id' => $id]);
        $this->db->from('pelanggan');
        $this->db->join('pembelian', 'id_pelanggan = kodeToko', 'left');
        // $this->db->order_by('kodeToko', 'ASC');
        $query = $this->db->get();
        return $query->row_array();
    }


    function get_pembelian($id)
    {
        $this->db->select('*');
        $this->db->where(['subcategory_id' => $id]);
        $this->db->from('pelanggan');
        $this->db->join('pembelian', 'id_pelanggan = kodeToko', 'left');
        $query = $this->db->get()->result_array();
        return $query;
    }
}
