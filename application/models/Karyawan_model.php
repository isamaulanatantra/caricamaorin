<?php

class Karyawan_model extends CI_Model
{
    function dataPegawai()
    {
        // $query = $this->db->get('karyawan');
        $query = $this->db->get_where('karyawan', array('status' => '1'));
        return $query;
    }

    public function tambahDataKaryawan()
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "jabatan" => $this->input->post('jabatan', true),
            "alamat" => $this->input->post('alamat', true),
            "kontak" => $this->input->post('kontak', true),
            "status" => ('1')
        ];

        $this->db->insert('karyawan', $data);
    }

    public function getAgendaById($id)
    {
        return $this->db->get_where('karyawan', ['id_pegawai' => $id])->row_array();
    }

    public function ubahDataTamu()
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "jabatan" => $this->input->post('jabatan', true),
            "alamat" => $this->input->post('alamat', true),
            "kontak" => $this->input->post('kontak', true)
        ];

        $this->db->where('id_pegawai', $this->input->post('id_pegawai'));
        $this->db->update('karyawan', $data);
    }

    public function ubahDataTamu2($id)
    {
        $data = [
            // "tanggalHapus" => "NOW()",
            "status" => '0'
        ];

        $this->db->set('tanggalHapus', 'NOW()', FALSE);
        $this->db->where('id_pegawai', $id);
        $this->db->update('karyawan', $data);
    }
}
