<?php

class Pengaturan_model extends CI_Model
{
    function getKota()
    {
        $this->db->select('*');
        $this->db->from('kota');
        $this->db->order_by('nama_kota', 'ASC');
        $query = $this->db->get();
        return $query;
    }

    function getKategoriP()
    {
        $this->db->select('*');
        $this->db->from('katpelanggan');
        $this->db->where('status = 1');
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query;
    }

    public function tambahDataProv()
    {
        $data = [
            "kode_prov" => $this->input->post('kodeProv', true),
            "nama_prov" => $this->input->post('namaProv', true)
        ];

        $this->db->insert('provinsi', $data);
    }

    public function addDataKota()
    {
        $data = [
            "kode_prov" => $this->input->post('provinsi', true),
            "kode_idpel" => $this->input->post('kode_idpel', true),
            "nama_kota" => $this->input->post('nama_kota', true)
        ];

        $this->db->insert('kota', $data);
    }

    public function addDataKatpel()
    {
        $data = [
            "kode" => $this->input->post('provinsi', true),
            "kategori" => $this->input->post('kode_idpel', true),
            "min" => $this->input->post('min', true),
            "status" => 1,
            "max" => $this->input->post('max', true)
        ];

        $this->db->insert('katpelanggan', $data);
    }

    function getProv()
    {
        $this->db->select('*');
        $this->db->from('provinsi');
        $this->db->order_by('kode_prov', 'ASC');
        $query = $this->db->get();
        return $query;
    }

    public function hapusDataProv($id)
    {
        $this->db->delete('provinsi', ['id_prov' => $id]);
    }

    public function hapusDataKatpel($id)
    {
        $data = [
            "status" => 0
        ];

        $this->db->where('id', $id);
        $this->db->update('katpelanggan', $data);
    }

    public function hapusDataKota($id)
    {
        $this->db->delete('kota', ['id_kota' => $id]);
    }

    public function getAgendaById($id)
    {
        return $this->db->get_where('kota', ['id_kota' => $id])->row_array();
    }

    public function getProvById($id)
    {
        return $this->db->get_where('provinsi', ['id_prov' => $id])->row_array();
    }

    public function getKategoriById($id)
    {
        return $this->db->get_where('katpelanggan', ['id' => $id])->row_array();
    }

    public function ubahDataKota()
    {
        $data = [
            "kode_idpel" => $this->input->post('idpel', true),
            "category_name" => $this->input->post('kotakab', true)
        ];

        $this->db->where('category_id', $this->input->post('id_pegawai'));
        $this->db->update('kota', $data);
    }

    public function ubahDataProv()
    {
        $data = [
            "kode_prov" => $this->input->post('kodeProv', true),
            "nama_prov" => $this->input->post('namaProv', true)
        ];

        $this->db->where('id_prov', $this->input->post('id'));
        $this->db->update('provinsi', $data);
    }

    public function updateKatpel()
    {
        $data = [
            "kode" => $this->input->post('provinsi', true),
            "kategori" => $this->input->post('kode_idpel', true),
            "min" => $this->input->post('min', true),
            "max" => $this->input->post('max', true)
        ];

        $this->db->where('id', $this->input->post('id_pegawai'));
        $this->db->update('katpelanggan', $data);
    }

    function cariProv($title)
    {
        $this->db->like('nama_prov', $title, 'both');
        $this->db->order_by('nama_prov', 'ASC');
        $this->db->limit(10);
        return $this->db->get('provinsi')->result();
    }
}
