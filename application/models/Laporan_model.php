<?php

class Laporan_model extends CI_Model
{
    function totalUangKeluar()
    {
        $this->db->select('*, SUM(total) AS TOTALUANG');
        $this->db->from('pembelian');
        $query = $this->db->get()->row_array();
        return $query;
    }

    function totalUangMasuk()
    {
        $this->db->select('*, SUM(dibayar) AS tdibayar');
        $this->db->from('pelanggan');
        $query = $this->db->get()->row_array();
        return $query;
    }


    function totalProdukKeluar()
    {
        $this->db->select('*, SUM(jumlah) AS jumlahproduk');
        $this->db->from('pembelian');
        $query = $this->db->get()->row_array();
        return $query;
    }
}
