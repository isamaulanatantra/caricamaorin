<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produksi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Produksi_model', 'mProd');
        $this->cek_status();
    }

    public function produk()
    {
        $data = array(
            'judul' => 'Pengaturan',
            'join' => $this->mProd->getProduk()->result(),
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array()
        );
        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('produksi/produk', $data);
        $this->load->view('templates/footer');
    }

    public function ubahProduk($id)
    {
        $data = array(
            'judul' => 'Pengaturan Produk',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'agenda' => $this->mProd->getProdukById($id)
        );

        $this->form_validation->set_rules('kategori', '', 'required', array('required' => 'Harus diisi.'));
        $this->form_validation->set_rules('kode', '', 'required', array('required' => 'Harus diisi.'));
        $this->form_validation->set_rules('produk', '', 'required', array('required' => 'Harus diisi.'));
        $this->form_validation->set_rules('satuan', '', 'required', array('required' => 'Harus diisi.'));

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('produksi/ubahProduk', $data);
            $this->load->view('templates/footer');
        } else {
            $this->mProd->ubahDataProduk();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('produksi/produk');
        }
    }

    public function addProduksi()
    {
        $data = array(
            'judul' => 'Pengaturan Produksi',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'kategori' => $this->mProd->ambilKategori()
        );
        $list = $this->db->get('bjadi')->row_array();
        $i = 0;
        foreach ($list as $row) {
            if ($i == 0) {
            }
        }

        $this->form_validation->set_rules('kategori', '', 'required|in_list[]', array('in_list' => 'Kategori tidak ada.'));
        // $this->form_validation->set_rules('kategori', '', 'required', array('required' => 'Harus diisi.'));
        $this->form_validation->set_rules('kode', '', 'required|is_unique[bjadi.kode]', array('is_unique' => 'Kode sudah digunakan.'));
        $this->form_validation->set_rules('produk', '', 'required|is_unique[bjadi.namaBarang]', array('is_unique' => 'Produk sudah ada.'));
        $this->form_validation->set_rules('satuan', '', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('templates/navbar');
            $this->load->view('produksi/addProduksi');
            $this->load->view('templates/footer');
        } else {
            $this->mProd->tambahDataProduk();
            $this->session->set_flashdata('flash', 'Ditambah');
            redirect('produksi/produk');
        }
    }

    public function hapusProduk($id)
    {
        $this->mProd->hapusDataProduk($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('produksi/produk');
    }
}
