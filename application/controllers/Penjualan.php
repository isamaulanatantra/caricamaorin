<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Penjualan_model', 'mPenjualan');
        $this->cek_status();
    }

    public function index()
    {
        $data = array(
            'judul' => 'Penjualan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'join' => $this->mPenjualan->get_products()->result()
        );

        $this->load->view('templates/header.php', $data);
        $this->load->view('templates/navbar.php', $data);
        $this->load->view('templates/sidebar.php', $data);
        $this->load->view('penjualan/index.php', $data);
        $this->load->view('templates/footer.php');
    }

    public function lihatTransaksi($id)
    {
        $data['judul'] = 'Ubah Pelanggan';
        $data['user'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $data['agenda'] = $this->mPenjualan->getPelangganById($id);
        $data['join'] = $this->mPenjualan->get_pembelian($id);
        // $data['produk'] = $this->mPelanggan->ambilProduk();

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('penjualan/detail', $data);
            $this->load->view('templates/footer');
        } else {
            // $this->mPelanggan->ubahDataPelanggan();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('penjualan');
        }
    }
}
