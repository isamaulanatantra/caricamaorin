<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Karyawan_model', 'mKaryawan');
        $this->cek_status();
    }

    public function index()
    {
        $data = array(
            'judul' => 'Karyawan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'pegawai' => $this->mKaryawan->dataPegawai()->result()
        );

        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('karyawan/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambahKaryawan()
    {
        $data['judul'] = 'Karyawan';
        $data['user'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $data['pegawai'] = $this->mKaryawan->dataPegawai()->result();

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('kontak', 'Kontak', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('templates/navbar');
            $this->load->view('karyawan/tambah');
            $this->load->view('templates/footer');
        } else {
            $this->mKaryawan->tambahDataKaryawan();
            $this->session->set_flashdata('flash', 'Ditambah');
            redirect('karyawan');
        }
    }

    public function ubahKaryawan($id)
    {
        $data['judul'] = 'Karyawan';
        $data['user'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $data['agenda'] = $this->mKaryawan->getAgendaById($id);

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('kontak', 'Kontak', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('karyawan/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->mKaryawan->ubahDataTamu();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('karyawan');
        }
    }

    public function hapusKaryawan($id)
    {
        $data['user'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $data['agenda'] = $this->mKaryawan->getAgendaById($id);
        $this->mKaryawan->ubahDataTamu2($id);
        $this->session->set_flashdata('flash', 'Diubah');
        redirect('karyawan');
    }
}
