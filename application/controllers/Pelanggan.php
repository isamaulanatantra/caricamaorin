<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelanggan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pelanggan_model', 'mPelanggan');
        $this->cek_status();
    }

    public function index()
    {
        $data = array(
            'judul' => 'Pelanggan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'join' => $this->mPelanggan->get_products()->result()
        );

        $this->load->view('templates/header.php', $data);
        $this->load->view('templates/navbar.php', $data);
        $this->load->view('templates/sidebar.php', $data);
        $this->load->view('pelanggan/index.php', $data);
        $this->load->view('templates/footer.php');
    }

    public function AddPelanggan()
    {
        $data = array(
            'judul' => 'Pelanggan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'nomorUrut' => $this->mPelanggan->noUrut(),
            'produk' => $this->mPelanggan->ambilProduk(),
        );

        $this->load->view('templates/header.php', $data);
        $this->load->view('templates/navbar.php', $data);
        $this->load->view('templates/sidebar.php', $data);
        $this->load->view('pelanggan/addPelanggan.php', $data);
        $this->load->view('templates/footer.php');
    }

    public function pelangganLama($id)
    {
        $data = array(
            'judul' => 'Pelanggan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'nomorUrut' => $this->mPelanggan->noUrut(),
            'agenda' => $this->mPelanggan->getPelangganById($id),
            'produk' => $this->mPelanggan->ambilProduk(),
        );

        $this->load->view('templates/header.php', $data);
        $this->load->view('templates/navbar.php', $data);
        $this->load->view('templates/sidebar.php', $data);
        $this->load->view('pelanggan/pelangganLama.php', $data);
        $this->load->view('templates/footer.php');
    }

    public function tambahFotodsdfas()
    {
        $data = array(
            'judul' => 'Pelanggan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array()
        );

        if ($this->input->post('submit')) { // Jika user menekan tombol Submit (Simpan) pada form
            // lakukan upload file dengan memanggil function upload yang ada di GambarModel.php
            $upload = $this->mPelanggan->upload();

            if ($upload['result'] == "success") { // Jika proses upload sukses
                // Panggil function save yang ada di GambarModel.php untuk menyimpan data ke database
                $this->mPelanggan->save($upload);
                $this->session->set_flashdata('flash', 'Ditambah');
                redirect('pelanggan'); // Redirect kembali ke halaman awal / halaman view data
            } else { // Jika proses upload gagal
                $data['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }


        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pelanggan/addPelanggan', $data);
        $this->load->view('templates/footer');
    }

    function tambahFoto()
    {
        $config['upload_path'] = './assets/upload/'; //path folder
        // $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        // $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
        $config['allowed_types'] = 'jpg|png|jpeg';
        // $config['max_size']    = '2048';
        $config['remove_space'] = TRUE;

        $this->upload->initialize($config);

        if (!empty($_FILES['inputFile']['name'])) {

            if ($this->upload->do_upload('inputFile')) {
                $gbr = $this->upload->data();

                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/upload/' . $gbr['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '60%';
                $config['width'] = 600;
                $config['height'] = 400;
                $config['new_image'] = './assets/upload/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $upload = $gbr['file_name'];
                // print_r($upload);
                // die();
                // $judul = $this->input->post('xjudul');
                $this->mPelanggan->save($upload);
                $this->session->set_flashdata('flash', 'Ditambah');
                redirect('pelanggan');
                // echo "Image berhasil diupload";
            }
        } else {
            $upload = 'default.jpg';
            $this->mPelanggan->save($upload);
            $this->session->set_flashdata('flash', 'Ditambah');
            redirect('pelanggan');
            // echo "Image yang diupload kosong";
        }
    }

    public function hapusPelanggan($id)
    {
        $this->mPelanggan->ubahPelanggan($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('pelanggan');
    }

    public function tambahBarang()
    {
        $data = array(
            'id' => '',
            'id_pelanggan' => $this->input->post('id_pelanggan'),
            'id_bjadi' => $this->input->post('id_bjadi'),
            'satuan' => $this->input->post('satuan'),
            'harga' => $this->input->post('harga'),
            'jumlah' => $this->input->post('jumlah'),
            'total' => $this->input->post('jumlah') * $this->input->post('harga'),
            'status' => 1,
        );

        $query = $this->db->insert('pembelian', $data);
        $insert_id = $this->db->insert_id();
        if ($query) {
            echo $insert_id;
        } else {
            echo "error";
        }
    }

    public function hapusBarang()
    {
        $id = $this->input->post('id');
        $this->db->delete('pembelian', ['id' => $id]);
        echo "success";
    }

    function get_autocomplete()
    {
        if (isset($_GET['term'])) {
            $result = $this->mPelanggan->search_blog($_GET['term']);
            if (count($result) > 0) {
                foreach ($result as $row)
                    $arr_result[] = array(
                        'label' =>  $row->nama_kota,
                        'description2' =>  $row->kode_idpel,
                    );
                echo json_encode($arr_result);
            }
        }
    }

    public function ubahPelanggan($id)
    {
        $data['judul'] = 'Detail Transaksi';
        $data['user'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $data['agenda'] = $this->mPelanggan->getPelangganById($id);
        $data['join'] = $this->mPelanggan->get_pembelian($id);
        $data['produk'] = $this->mPelanggan->ambilProduk();

        $this->form_validation->set_rules('cicilan', '', 'required');
        $this->form_validation->set_rules('cicilanLama', '', 'required');
        // $this->form_validation->set_rules('nama', '', 'required');
        // $this->form_validation->set_rules('namaPelanggan', '', 'required');
        // $this->form_validation->set_rules('kota', '', 'required');
        // $this->form_validation->set_rules('alamat', '', 'required');
        // $this->form_validation->set_rules('telp', '', 'required');
        // $this->form_validation->set_rules('numberWA', '', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('pelanggan/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->mPelanggan->ubahDataPelanggan();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('pelanggan/ubahPelanggan/' . $id);
        }
    }

    public function lunas($id)
    {
        $this->mPelanggan->lunasPelanggan($id);
        $this->session->set_flashdata('flash', 'Diubah');
        redirect('pelanggan');
    }
}
