<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Laporan_model', 'mLaporan');
        $this->cek_status();
    }

    public function index()
    {
        $data = array(
            'judul' => 'Laporan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'uangkeluar' => $this->mLaporan->totalUangKeluar(),
            'umasuk' => $this->mLaporan->totalUangMasuk(),
            'pKeluar' => $this->mLaporan->totalProdukKeluar(),
            // 'join' => $this->mPelanggan->get_products()->result()
        );

        $this->load->view('templates/header.php', $data);
        $this->load->view('templates/navbar.php', $data);
        $this->load->view('templates/sidebar.php', $data);
        $this->load->view('laporan/index.php', $data);
        $this->load->view('templates/footer.php');
    }
}
