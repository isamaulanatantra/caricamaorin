<?php

class Category extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model');
        $this->cek_status();
    }

    public function index()
    {
        $data['categories'] = $this->category_model->get('category');
        $data['user'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $data['judul'] = 'Carica Maorin';

        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('home/index', $data);
        $this->load->view('templates/footer', $data);
    }
}
