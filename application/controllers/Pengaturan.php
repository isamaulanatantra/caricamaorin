<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengaturan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pengaturan_model', 'mSetting');
        $this->load->model('Produksi_model', 'mProd');
        $this->cek_status();
    }

    public function index()
    {
        $data = array(
            'judul' => 'Pengaturan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array()
        );
        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pengaturan/index', $data);
        $this->load->view('templates/footer');
    }

    public function kota()
    {
        $data = array(
            'judul' => 'Pengaturan Kota',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'join' => $this->mSetting->getKota()->result()
        );
        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pengaturan/kota/index', $data);
        $this->load->view('templates/footer');
    }

    public function ubahKota($id)
    {
        $data = array(
            'judul' => 'Pengaturan Kota',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'agenda' => $this->mSetting->getAgendaById($id)
        );

        $this->form_validation->set_rules('idpel', 'Kode', 'required');
        $this->form_validation->set_rules('kotakab', 'Kota / Kab', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('pengaturan/kota/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->mSetting->ubahDataKota();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('pengaturan/kota');
        }
    }

    public function addProv()
    {
        $data = array(
            'judul' => 'Pengaturan Provinsi',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array()
        );

        $this->form_validation->set_rules('kodeProv', '', 'required|is_unique[provinsi.kode_prov]', array('is_unique' => 'Kode Provinsi sudah digunakan.'));
        $this->form_validation->set_rules('namaProv', '', 'required|is_unique[provinsi.nama_prov]', array('is_unique' => 'Nama Provinsi sudah digunakan.'));

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('templates/navbar');
            $this->load->view('pengaturan/provinsi/add');
            $this->load->view('templates/footer');
        } else {
            $this->mSetting->tambahDataProv();
            $this->session->set_flashdata('flash', 'Ditambah');
            redirect('pengaturan/provinsi');
        }
    }

    public function ubahProv($id)
    {
        $data = array(
            'judul' => 'Pengaturan Provinsi',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'agenda' => $this->mSetting->getProvById($id)
        );

        $this->form_validation->set_rules('kodeProv', 'Kode Provinsi', 'required');
        $this->form_validation->set_rules('namaProv', 'Nama Provinsi', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('pengaturan/provinsi/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->mSetting->ubahDataProv();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('pengaturan/provinsi');
        }
    }

    public function provinsi()
    {
        $data = array(
            'judul' => 'Pengaturan Provinsi',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'provinsi' => $this->mSetting->getProv()->result()
        );

        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pengaturan/provinsi/index', $data);
        $this->load->view('templates/footer');
    }

    public function hapusProv($id)
    {
        $this->mSetting->hapusDataProv($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('pengaturan/provinsi');
    }

    public function hapusKota($id)
    {
        $this->mSetting->hapusDataKota($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('pengaturan/kota');
    }

    public function hapusKatpel($id)
    {
        $this->mSetting->hapusDataKatpel($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('pengaturan/pelangganKategori');
    }

    public function addKota()
    {
        $data = array(
            'judul' => 'Pengaturan Kota',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array()
        );

        $this->form_validation->set_rules('provinsi', '', 'required', array('required' => 'Harus diisi!'));
        $this->form_validation->set_rules('kode_idpel', '', 'required|is_unique[kota.kode_idpel]', array('is_unique' => 'Kode Pelanggan sudah digunakan.', 'required' => 'Harus diisi!'));
        $this->form_validation->set_rules('nama_kota', '', 'required|is_unique[kota.nama_kota]', array('is_unique' => 'Kota / Kab sudah ada.', 'required' => 'Harus diisi!'));

        if ($this->form_validation->run() == false) {
            // $this->load->view('coba.html', $data);
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('pengaturan/kota/add', $data);
            $this->load->view('templates/footer');
        } else {
            $this->mSetting->addDataKota();
            $this->session->set_flashdata('flash', 'Ditambah');
            redirect('pengaturan/kota');
        }
    }

    function autoProv()
    {
        if (isset($_GET['term'])) {
            $result = $this->mSetting->cariProv($_GET['term']);
            if (count($result) > 0) {
                foreach ($result as $row)
                    $arr_result[] = array(
                        'label' =>  $row->nama_prov,
                        'description2' =>  $row->kode_prov,
                    );
                echo json_encode($arr_result);
            }
        }
    }

    public function pelangganKategori()
    {
        $data = array(
            'judul' => 'Pengaturan Kategori Pelanggan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'kategori' => $this->mSetting->getKategoriP()->result()
        );

        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pengaturan/katpel/index', $data);
        $this->load->view('templates/footer');
    }

    public function addKatpel()
    {
        $data = array(
            'judul' => 'Pengaturan Kategori Pelanggan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array()
        );

        $this->form_validation->set_rules('provinsi', '', 'required|is_unique[katpelanggan.kode]', array('is_unique' => 'Kode Kategori sudah digunakan.'));
        $this->form_validation->set_rules('kode_idpel', '', 'required|is_unique[katpelanggan.kategori]', array('is_unique' => 'Kategori sudah ada.'));
        $this->form_validation->set_rules('min', '', 'required', array('required' => 'Harus Diisi'));
        $this->form_validation->set_rules('max', '', 'required|greater_than_equal_to[' . $this->input->post('min') . ']', array('greater_than_equal_to' => 'Harga Maks harus lebih tinggi dari harga Min.'));

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('pengaturan/katpel/add', $data);
            $this->load->view('templates/footer');
        } else {
            $this->mSetting->addDataKatpel();
            $this->session->set_flashdata('flash', 'Ditambah');
            redirect('pengaturan/pelangganKategori');
        }
    }

    public function ubahKatpel($id)
    {
        $data = array(
            'judul' => 'Pengaturan Kategori Pelanggan',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'agenda' => $this->mSetting->getKategoriById($id)
        );

        $this->form_validation->set_rules('provinsi', '', 'required|is_unique[katpelanggan.kode]', array('is_unique' => 'Kode Kategori sudah digunakan.'));
        $this->form_validation->set_rules('kode_idpel', '', 'required|is_unique[katpelanggan.kategori]', array('is_unique' => 'Kategori sudah ada.'));
        $this->form_validation->set_rules('min', '', 'required', array('required' => 'Harus Diisi'));
        $this->form_validation->set_rules('max', '', 'required|greater_than_equal_to[' . $this->input->post('min') . ']', array('greater_than_equal_to' => 'Harga Maks harus lebih tinggi dari harga Min.'));

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('pengaturan/katpel/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->mSetting->updateKatpel();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('pengaturan/pelangganKategori');
        }
    }

    // Produk Produksi
    public function produk()
    {
        $data = array(
            'judul' => 'Pengaturan',
            'join' => $this->mProd->getProduk()->result(),
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array()
        );
        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('produksi/produk', $data);
        $this->load->view('templates/footer');
    }

    public function ubahProduk($id)
    {
        $data = array(
            'judul' => 'Pengaturan Produk',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'agenda' => $this->mProd->getAgendaById($id)
        );

        $this->form_validation->set_rules('kategori', '', 'required', array('required' => 'Harus diisi.'));
        $this->form_validation->set_rules('kode', '', 'required|is_unique[bjadi.kode]', array('is_unique' => 'Kode sudah digunakan.'));
        $this->form_validation->set_rules('produk', '', 'required|is_unique[bjadi.namaBarang]', array('is_unique' => 'Produk sudah ada.'));
        $this->form_validation->set_rules('satuan', '', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('produksi/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->mProd->ubahDataKota();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('pengaturan/produk');
        }
    }

    public function addProduksi()
    {
        $data = array(
            'judul' => 'Pengaturan Produksi',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
        );

        $this->form_validation->set_rules('kategori', '', 'required', array('required' => 'Harus diisi.'));
        $this->form_validation->set_rules('kode', '', 'required|is_unique[bjadi.kode]', array('is_unique' => 'Kode sudah digunakan.'));
        $this->form_validation->set_rules('produk', '', 'required|is_unique[bjadi.namaBarang]', array('is_unique' => 'Produk sudah ada.'));
        $this->form_validation->set_rules('satuan', '', 'required');
        $this->form_validation->set_rules('min', '', 'required');
        $this->form_validation->set_rules('max', '', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('templates/navbar');
            $this->load->view('produksi/addProduksi');
            $this->load->view('templates/footer');
        } else {
            $this->mProd->tambahDataProduk();
            $this->session->set_flashdata('flash', 'Ditambah');
            redirect('pengaturan/produk');
        }
    }

    public function hapusProduk($id)
    {
        $this->mProd->hapusDataProv($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('pengaturan/produk');
    }

    // Produk Kategori
    public function kategori()
    {
        $data = array(
            'judul' => 'Pengaturan',
            'join' => $this->mProd->getProduk()->result(),
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array()
        );
        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('produksi/kategori', $data);
        $this->load->view('templates/footer');
    }

    // Produk Bahan Baku
    public function bBaku()
    {
        $data = array(
            'judul' => 'Pengaturan',
            'join' => $this->mProd->getBaku()->result(),
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array()
        );
        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('produksi/bahanbaku/index', $data);
        $this->load->view('templates/footer');
    }

    public function addBaku()
    {
        $data = array(
            'judul' => 'Pengaturan Produksi',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
        );

        $this->form_validation->set_rules('kategori', '', 'required', array('required' => 'Harus diisi.'));
        $this->form_validation->set_rules('kode', '', 'required|is_unique[bbaku.kode]', array('is_unique' => 'Kode sudah digunakan.'));
        $this->form_validation->set_rules('produk', '', 'required|is_unique[bbaku.produk]', array('is_unique' => 'Produk sudah ada.'));
        $this->form_validation->set_rules('satuan', '', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('templates/navbar');
            $this->load->view('produksi/bahanbaku/add');
            $this->load->view('templates/footer');
        } else {
            $this->mProd->tambahDataBaku();
            $this->session->set_flashdata('flash', 'Ditambah');
            redirect('pengaturan/bBaku');
        }
    }

    public function hapusBaku($id)
    {
        $this->mProd->hapusDataBaku($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('pengaturan/bBaku');
    }

    public function ubahBaku($id)
    {
        $data = array(
            'judul' => 'Pengaturan Produk',
            'user' => $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array(),
            'agenda' => $this->mProd->getBakuById($id)
        );

        $this->form_validation->set_rules('kategori', '', 'required');
        $this->form_validation->set_rules('kode', '', 'required');
        $this->form_validation->set_rules('produk', '', 'required');
        $this->form_validation->set_rules('satuan', '', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navbar', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('produksi/bahanbaku/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->mProd->ubahDataBaku();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('pengaturan/bBaku');
        }
    }
}
